create_product.php
<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['nr_pomiaru'])) {
    $nr_pomiaru = $_GET['nr_pomiaru'];
    $fk_id_punkt_pomiarowy = $_GET['fk_id_punkt_pomiarowy'];
    $fk_id_ap = $_GET['fk_id_ap'];
    $fk_id_urzadzenie = $_GET['fk_id_urzadzenie'];
    $rssi = $_GET['rssi'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
    // mysql inserting a new row
    //autoinkrementację można pominąć
    $result = mysql_query("INSERT INTO DANE_SUROWE(nr_pomiaru, fk_id_punkt_pomiarowy, fk_id_ap, fk_id_urzadzenie, rssi)
            VALUES('$nr_pomiaru', '$fk_id_punkt_pomiarowy', '$fk_id_ap', '$fk_id_urzadzenie', '$rssi')");
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>