create_product.php
<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['fk_id_punkt_pomiarowy'])) {
 
    $fk_id_punkt_pomiarowy = $_GET['fk_id_punkt_pomiarowy'];
    $fk_id_ap = $_GET['fk_id_ap'];
    $fk_id_urzadzenie = $_GET['fk_id_urzadzenie'];
    $min_rssi = $_GET['min_rssi'];
    $max_rssi = $_GET['max_rssi'];
    $sredniaPL3s = $_GET['sredniaPL3s'];
    $sredniaMIN3s = $_GET['sredniaMIN3s'];
    $mediana = $_GET['mediana'];
    $dominanta = $_GET['dominanta'];
    $srednia_rssi = $_GET['srednia_rssi'];
      
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
    // mysql inserting a new row
    
    $result = mysql_query("INSERT INTO DANE_ZAGREGOWANE(fk_id_punkt_pomiarowy, fk_id_ap, fk_id_urzadzenie, min_rssi, max_rssi, sredniaPL3s, sredniaMIN3s, mediana, dominanta, srednia_rssi)
            VALUES('$fk_id_punkt_pomiarowy', '$fk_id_ap', '$fk_id_urzadzenie', '$min_rssi', '$max_rssi', '$sredniaPL3s','$sredniaMIN3s','$mediana','$dominanta','$srednia_rssi')");
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>