create_product.php
<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['nazwa'])) {
 
    $nazwa = $_GET['nazwa'];
    $rozmiar_x = $_GET['rozmiar_x'];
    $rozmiar_y = $_GET['rozmiar_y'];
    $liczba_pomiarow = $_GET['liczba_pomiarow'];
    $poziomy_odstep_pomiaru = $_GET['poziomy_odstep_pomiaru'];
    $pionowy_odstep_pomiaru = $_GET['pionowy_odstep_pomiaru'];
    $fk_id_budynek = $_GET['fk_id_budynek'];
    $id_N = $_GET['id_N'];
    $id_S = $_GET['id_S'];
    $id_E = $_GET['id_E'];
    $id_W = $_GET['id_W'];
    
    
    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
    // mysql inserting a new row
    $result = mysql_query("INSERT INTO POMIESZCZENIE(nazwa, fk_id_budynek, rozmiar_x, rozmiar_y, liczba_pomiarow,poziomy_odstep_pomiaru,
        pionowy_odstep_pomiaru, id_N, id_S, id_E, id_W)
            VALUES('$nazwa', '$fk_id_budynek', '$rozmiar_x', '$rozmiar_y', '$liczba_pomiarow','$poziomy_odstep_pomiaru','$pionowy_odstep_pomiaru', '$id_N', '$id_S', '$id_E', '$id_W')");
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>