create_product.php
<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['fk_id_pomieszczenie'])) {
 
    $fk_id_pomieszczenie = $_GET['fk_id_pomieszczenie'];
    $fk_id_budynek = $_GET['fk_id_budynek'];
    $pozycja_x = $_GET['pozycja_x'];
    $pozycja_y = $_GET['pozycja_y'];
    $graniczny = $_GET['graniczny'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';
 
    // connecting to db
    $db = new DB_CONNECT();
 
   // mysql inserting a new row
    $result = mysql_query("INSERT INTO PUNKT_POMIAROWY(fk_id_pomieszczenie, fk_id_budynek, pozycja_x, pozycja_y, graniczny) 
            VALUES('$fk_id_pomieszczenie', '$fk_id_budynek', '$pozycja_x', '$pozycja_y', '$graniczny')");
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>