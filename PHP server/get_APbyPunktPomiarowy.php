<?php
 
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();

 
// get all products from products table

if (isset($_GET["fk_id_punkt_pomiarowy"])) {

    $fk_id_punkt_pomiarowy = $_GET['fk_id_punkt_pomiarowy'];
    // get a product from products table
    $result = mysql_query("SELECT DISTINCT fk_id_ap FROM DANE_SUROWE WHERE fk_id_punkt_pomiarowy = $fk_id_punkt_pomiarowy");
    //$result = mysql_query("SELECT * FROM DANE_SUROWE WHERE mac = $mac AND pozycja_x = $pozycja_x AND pozycja_y = $pozycja_y AND fk_id_pomieszczenie = $fk_id_pomieszczenie AND fk_id_budynek = $fk_id_budynek" );
 
    // check for empty result
    if (mysql_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["dane_surowe"] = array();

        while ($row = mysql_fetch_array($result)) {
        // temp user array
            $dane_surowe = array();
            $dane_surowe["fk_id_ap"] = $row["fk_id_ap"];
            
            
            // push single product into final response array
            array_push($response["dane_surowe"], $dane_surowe);
        }
        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No products found";

        // echo no users JSON
        echo json_encode($response);
    }    
 

}
else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}

?>