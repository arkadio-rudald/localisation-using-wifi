<?php
 
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();

 
// get all products from products table

if (isset($_GET["fk_id_budynek"])) {

    $fk_id_budynek = $_GET['fk_id_budynek'];
    // get a product from products table
    $result = mysql_query("SELECT * FROM POMIESZCZENIE WHERE fk_id_budynek = $fk_id_budynek");
    //$result = mysql_query("SELECT * FROM DANE_SUROWE WHERE mac = $mac AND pozycja_x = $pozycja_x AND pozycja_y = $pozycja_y AND fk_id_pomieszczenie = $fk_id_pomieszczenie AND fk_id_budynek = $fk_id_budynek" );
 
    // check for empty result
    if (mysql_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["pomieszczenie"] = array();

        while ($row = mysql_fetch_array($result)) {
            // temp user array
            
            $pomieszczenie = array();
            $pomieszczenie["id"] = $row["id"];
            $pomieszczenie["nazwa"] = $row["nazwa"];
            $pomieszczenie["fk_id_budynek"] = $row["fk_id_budynek"];
            $pomieszczenie["rozmiar_x"] = $row["rozmiar_x"];
            $pomieszczenie["rozmiar_y"] = $row["rozmiar_y"];
            $pomieszczenie["liczba_pomiarow"] = $row["liczba_pomiarow"];
            $pomieszczenie["poziomy_odstep_pomiaru"] = $row["poziomy_odstep_pomiaru"];
            $pomieszczenie["pionowy_odstep_pomiaru"] = $row["pionowy_odstep_pomiaru"];
            $pomieszczenie["id_N"] = $row["id_N"];
            $pomieszczenie["id_S"] = $row["id_S"];
            $pomieszczenie["id_E"] = $row["id_E"];
            $pomieszczenie["id_W"] = $row["id_W"];


            // push single product into final response array
            array_push($response["pomieszczenie"], $pomieszczenie);
        }
        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No products found";

        // echo no users JSON
        echo json_encode($response);
    }    
 

}
else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}

?>