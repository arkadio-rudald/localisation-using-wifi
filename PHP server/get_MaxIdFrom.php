<?php
 
/*
 * Following code will list all the products
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();

if (isset($_GET["tablename"])) 
{
    $tablename = $_GET["tablename"];         
    $result = mysql_query("SELECT MAX(id) FROM $tablename ");
    
    //$result = mysql_query("SELECT * FROM DANE_SUROWE WHERE mac = $mac AND pozycja_x = $pozycja_x AND pozycja_y = $pozycja_y AND fk_id_pomieszczenie = $fk_id_pomieszczenie AND fk_id_budynek = $fk_id_budynek" );
 
    // check for empty result
    if (mysql_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["max"] = array();

        while ($row = mysql_fetch_array($result)) {
            // temp user array
            $max = array();
            $max["MAX(id)"] = $row["MAX(id)"];
            
            // push single product into final response array
            array_push($response["max"], $max);
        }
        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "Nie znalazlem budynkow";

        // echo no users JSON
        echo json_encode($response);
    }    
}
 
?>