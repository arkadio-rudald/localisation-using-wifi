package com.example.arkadio.wifimapping;

public class AP
{
    private int id;
    private String ssid;
    private String mac;
    private int frequency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}

/* Location:           C:\Users\Arkadio\Desktop\APKtoJava_RC22\tools\classes-dex2jar.jar
 * Qualified Name:     com.example.arkadio.naviwifi.AP
 * JD-Core Version:    0.6.0
 */