package com.example.arkadio.wifimapping;

public class Budynek
{
  private int id;
  private String nazwa;

  public int getId()
  {
    return this.id;
  }

  public String getNazwa()
  {
    return this.nazwa;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setNazwa(String paramString)
  {
    this.nazwa = paramString;
  }
}

/* Location:           C:\Users\Arkadio\Desktop\APKtoJava_RC22\tools\classes-dex2jar.jar
 * Qualified Name:     com.example.arkadio.naviwifi.Budynek
 * JD-Core Version:    0.6.0
 */