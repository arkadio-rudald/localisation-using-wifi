package com.example.arkadio.wifimapping;

public class DaneSurowe
{
    private int id;
    private int nr_pomiaru;
    private int rssi;
    private int fk_id_punkt_pomiarowy;
    private int fk_id_ap;
    private int fk_id_urzadzenie;


    public int getFk_id_urzadzenie() {
        return fk_id_urzadzenie;
    }

    public void setFk_id_urzadzenie(int fk_id_urzadzenie) {
        this.fk_id_urzadzenie = fk_id_urzadzenie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNr_pomiaru() {
        return nr_pomiaru;
    }

    public void setNr_pomiaru(int nr_pomiaru) {
        this.nr_pomiaru = nr_pomiaru;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getFk_id_punkt_pomiarowy() {
        return fk_id_punkt_pomiarowy;
    }

    public void setFk_id_punkt_pomiarowy(int fk_id_punkt_pomiarowy) {
        this.fk_id_punkt_pomiarowy = fk_id_punkt_pomiarowy;
    }

    public int getFk_id_ap() {
        return fk_id_ap;
    }

    public void setFk_id_ap(int fk_id_ap) {
        this.fk_id_ap = fk_id_ap;
    }
}