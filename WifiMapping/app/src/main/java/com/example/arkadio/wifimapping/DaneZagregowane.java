package com.example.arkadio.wifimapping;

/**
 * Created by Arkadio on 2015-01-15.
 */
public class DaneZagregowane {
    private int id;
    private int fk_id_punkt_pomiarowy;
    private int fk_id_ap;
    private int fk_id_urzadzenie;
    private int min_rssi;
    private int max_rssi;
    private int dominanta;
    private int mediana;
    private float sredniaMIN3s;
    private float sredniaPL3s;
    private float srednia_rssi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFk_id_punkt_pomiarowy() {
        return fk_id_punkt_pomiarowy;
    }

    public void setFk_id_punkt_pomiarowy(int fk_id_punkt_pomiarowy) {
        this.fk_id_punkt_pomiarowy = fk_id_punkt_pomiarowy;
    }

    public int getFk_id_ap() {
        return fk_id_ap;
    }

    public void setFk_id_ap(int fk_id_ap) {
        this.fk_id_ap = fk_id_ap;
    }

    public int getFk_id_urzadzenie() {
        return fk_id_urzadzenie;
    }

    public void setFk_id_urzadzenie(int fk_id_urzadzenie) {
        this.fk_id_urzadzenie = fk_id_urzadzenie;
    }

    public int getMin_rssi() {
        return min_rssi;
    }

    public void setMin_rssi(int min_rssi) {
        this.min_rssi = min_rssi;
    }

    public int getMax_rssi() {
        return max_rssi;
    }

    public void setMax_rssi(int max_rssi) {
        this.max_rssi = max_rssi;
    }

    public int getDominanta() {
        return dominanta;
    }

    public void setDominanta(int dominanta) {
        this.dominanta = dominanta;
    }

    public int getMediana() {
        return mediana;
    }

    public void setMediana(int mediana) {
        this.mediana = mediana;
    }

    public float getSredniaMIN3s() {
        return sredniaMIN3s;
    }

    public void setSredniaMIN3s(float sredniaMIN3s) {
        this.sredniaMIN3s = sredniaMIN3s;
    }

    public float getSredniaPL3s() {
        return sredniaPL3s;
    }

    public void setSredniaPL3s(float sredniaPL3s) {
        this.sredniaPL3s = sredniaPL3s;
    }

    public float getSrednia_rssi() {
        return srednia_rssi;
    }

    public void setSrednia_rssi(float srednia_rssi) {
        this.srednia_rssi = srednia_rssi;
    }
}
