package com.example.arkadio.wifimapping;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class ExternalDatabase
{
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    public void createFieldBudynek(Budynek budynek)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value = new BasicNameValuePair("nazwa", budynek.getNazwa());
        params.add(value);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_Budynek.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldPomieszczenie(Pomieszczenie room)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value = new BasicNameValuePair("nazwa", room.getNazwa());
        params.add(value);
        value = new BasicNameValuePair("fk_id_budynek", Integer.toString(room.getFk_id_budynek()));
        params.add(value);
        value = new BasicNameValuePair("rozmiar_x", Float.toString(room.getRozmiar_x()));
        params.add(value);
        value = new BasicNameValuePair("rozmiar_y", Float.toString(room.getRozmiar_y()));
        params.add(value);
        value = new BasicNameValuePair("liczba_pomiarow", Integer.toString(room.getLiczba_pomiarow()));
        params.add(value);
        value = new BasicNameValuePair("poziomy_odstep_pomiaru", Float.toString(room.getPoziomy_odstep_pomiaru()));
        params.add(value);
        value = new BasicNameValuePair("pionowy_odstep_pomiaru", Float.toString(room.getPionowy_odstep_pomiaru()));
        params.add(value);
        value = new BasicNameValuePair("id_N", Integer.toString(room.getId_N()));
        params.add(value);
        value = new BasicNameValuePair("id_S", Integer.toString(room.getId_S()));
        params.add(value);
        value = new BasicNameValuePair("id_W", Integer.toString(room.getId_W()));
        params.add(value);
        value = new BasicNameValuePair("id_E", Integer.toString(room.getId_E()));
        params.add(value);


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_Pomieszczenie.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldUrzadzenie(Urzadzenie device)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value = new BasicNameValuePair("mac", device.getMac());
        params.add(value);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_Urzadzenie.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldPunktPomiarowy(PunktPomiarowy pkt)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value;
        value = new BasicNameValuePair("fk_id_budynek", Integer.toString(pkt.getFk_id_budynek()));
        params.add(value);
        value = new BasicNameValuePair("fk_id_pomieszczenie", Integer.toString(pkt.getFk_id_pomieszczenie()));
        params.add(value);
        value = new BasicNameValuePair("pozycja_x", Float.toString(pkt.getPozycja_x()));
        params.add(value);
        value = new BasicNameValuePair("pozycja_y", Float.toString(pkt.getPozycja_y()));
        params.add(value);
        value = new BasicNameValuePair("graniczny", Integer.toString(pkt.getGraniczny()));
        params.add(value);


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_PunktPomiarowy.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldAP(AP ap)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value;
        value = new BasicNameValuePair("ssid", ap.getSsid());
        params.add(value);
        value = new BasicNameValuePair("mac", ap.getMac());
        params.add(value);
        value = new BasicNameValuePair("frequency", Integer.toString(ap.getFrequency()));
        params.add(value);


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_AP.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldDaneZagregowane(DaneZagregowane dane)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value;
        value = new BasicNameValuePair("fk_id_punkt_pomiarowy", Integer.toString(dane.getFk_id_punkt_pomiarowy()));
        params.add(value);
        value = new BasicNameValuePair("fk_id_ap", Integer.toString(dane.getFk_id_ap()));
        params.add(value);
        value = new BasicNameValuePair("fk_id_urzadzenie", Integer.toString(dane.getFk_id_urzadzenie()));
        params.add(value);
        value = new BasicNameValuePair("min_rssi", Integer.toString(dane.getMin_rssi()));
        params.add(value);
        value = new BasicNameValuePair("max_rssi", Integer.toString(dane.getMax_rssi()));
        params.add(value);
        value = new BasicNameValuePair("mediana", Integer.toString(dane.getMediana()));
        params.add(value);
        value = new BasicNameValuePair("dominanta", Integer.toString(dane.getDominanta()));
        params.add(value);
        value = new BasicNameValuePair("srednia_rssi", Float.toString(dane.getSrednia_rssi()));
        params.add(value);
        value = new BasicNameValuePair("sredniaPL3s", Float.toString(dane.getSredniaPL3s()));
        params.add(value);
        value = new BasicNameValuePair("sredniaMIN3s", Float.toString(dane.getSredniaPL3s()));
        params.add(value);


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_DaneZagregowane.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }

    public void createFieldDaneSurowe(DaneSurowe dane)
    {
        ArrayList params = new ArrayList();

        BasicNameValuePair value;
        value = new BasicNameValuePair("fk_id_punkt_pomiarowy", Integer.toString(dane.getFk_id_punkt_pomiarowy()));
        params.add(value);
        value = new BasicNameValuePair("fk_id_ap", Integer.toString(dane.getFk_id_ap()));
        params.add(value);
        value = new BasicNameValuePair("fk_id_urzadzenie", Integer.toString(dane.getFk_id_urzadzenie()));
        params.add(value);
        value = new BasicNameValuePair("nr_pomiaru", Integer.toString(dane.getNr_pomiaru()));
        params.add(value);
        value = new BasicNameValuePair("rssi", Integer.toString(dane.getRssi()));
        params.add(value);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/add_DaneSurowe.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }
    }


    public List<PunktPomiarowy> getPunktyPomiarowebyBudynek(int id_budynek) throws IOException {
        List<PunktPomiarowy> punkty = new ArrayList<PunktPomiarowy>();
        String fk_id_budynek = Integer.toString(id_budynek);

        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_budynek", fk_id_budynek);
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String paramString = URLEncodedUtils.format(params, "utf-8");

        String url = "http://bycniebytem.cba.pl/inz_php/get_ListPunktyPomiaroweByBudynek.php";
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("punkt_pomiarowy").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("punkt_pomiarowy"))
                        {
                            PunktPomiarowy pomiar = new PunktPomiarowy();
                            pomiar.setId(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("id").toString()));
                            pomiar.setFk_id_pomieszczenie(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_pomieszczenie").toString()));
                            pomiar.setFk_id_budynek(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_budynek").toString()));
                            pomiar.setPozycja_x(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_x").toString()));
                            pomiar.setPozycja_y(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_y").toString()));
                            pomiar.setGraniczny(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("graniczny").toString()));
                            punkty.add(pomiar);
                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return punkty;
    }

    public List<Integer> getAPbyPunktPomiarowy(int id_punkt_pomiarowy)
    {
        List<Integer> idList = new ArrayList<Integer>();

        String pkt = Integer.toString(id_punkt_pomiarowy);
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_punkt_pomiarowy", pkt);
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_APbyPunktPomiarowy.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("dane_surowe").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("dane_surowe"))
                        {
                            idList.add((Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_ap").toString())));
                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        return idList;
    }


    public List<AP> getAPbyBudynek(int id_budynek) throws IOException {
        List<Integer> routery = new ArrayList<Integer>();

        List<PunktPomiarowy> listPunktPomiarowy;

        listPunktPomiarowy = getPunktyPomiarowebyBudynek(id_budynek);

        for(int m = 0; m < listPunktPomiarowy.size(); m++)
        {
            List<Integer> aps;
            aps = getAPbyPunktPomiarowy(listPunktPomiarowy.get(m).getId());
            boolean exist = false;
            for (int n = 0; n < aps.size(); n++)
            {
                for (int check = 0; check < routery.size(); check++)
                {
                    if (aps.get(n) == routery.get(check))
                    {
                        exist = true;
                        break;
                    }
                    exist = false;
                }
                if (!exist)
                {
                    routery.add(aps.get(n));
                }
            }
        }

        List<AP> apy = new ArrayList<AP>();
        for(int i = 0; i < routery.size(); i++)
        {
            apy.add(getAPById(routery.get(i)));
        }

        return apy;
    }
    /*
                dane = exConnection.getDaneSuroweList();
                pomieszczenia = exConnection.getPomieszczenieList();
                punktyPomiarowe = exConnection.getPunktyPomiaroweList();
     */

    public AP getAPById(int id)
    {
        AP accesspoint = new AP();

        String id_ap = Integer.toString(id);
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("id", id_ap);
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String paramString = URLEncodedUtils.format(params, "utf-8");

        String url = "http://bycniebytem.cba.pl/inz_php/get_APById.php";
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("accesspoint").length(); i++) {
                        if (jObj.names().get(1).toString().equals("accesspoint")) {
                            accesspoint.setId(Integer.parseInt(jObj.getJSONArray("accesspoint").getJSONObject(i).get("id").toString()));
                            accesspoint.setSsid(jObj.getJSONArray("accesspoint").getJSONObject(i).get("ssid").toString());
                            accesspoint.setMac(jObj.getJSONArray("accesspoint").getJSONObject(i).get("mac").toString());
                            accesspoint.setFrequency(Integer.parseInt(jObj.getJSONArray("accesspoint").getJSONObject(i).get("frequency").toString()));
                            Log.d("AP", accesspoint.getMac());
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return accesspoint;

    }

    public List<AP> getAPList() throws IOException {
        List<AP> routery = new ArrayList<AP>();

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListAP.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("accesspoint").length(); i++) {
                        if (jObj.names().get(1).toString().equals("accesspoint")) {
                            AP accesspoint = new AP();
                            accesspoint.setId(Integer.parseInt(jObj.getJSONArray("accesspoint").getJSONObject(i).get("id").toString()));
                            accesspoint.setSsid(jObj.getJSONArray("accesspoint").getJSONObject(i).get("ssid").toString());
                            accesspoint.setMac(jObj.getJSONArray("accesspoint").getJSONObject(i).get("mac").toString());
                            accesspoint.setFrequency(Integer.parseInt(jObj.getJSONArray("accesspoint").getJSONObject(i).get("frequency").toString()));
                            routery.add(accesspoint);
                            Log.d("AP", accesspoint.getMac());
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return routery;
    }

    public List<Urzadzenie> getUrzadzenieList() throws IOException {
        List<Urzadzenie> urzadzenieList = new ArrayList<Urzadzenie>();

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListUrzadzenie.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("urzadzenie").length(); i++) {
                        if (jObj.names().get(1).toString().equals("urzadzenie")) {
                            Urzadzenie urzadzenie = new Urzadzenie();
                            urzadzenie.setId(Integer.parseInt(jObj.getJSONArray("urzadzenie").getJSONObject(i).get("id").toString()));
                            urzadzenie.setMac(jObj.getJSONArray("urzadzenie").getJSONObject(i).get("mac").toString());
                            urzadzenieList.add(urzadzenie);
                            Log.d("Urzadzenie", urzadzenie.getMac());
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return urzadzenieList;
    }

    public List<DaneSurowe> getDaneSuroweList() throws IOException {
        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListDaneSurowe.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("dane_surowe").length(); i++) {
                        if (jObj.names().get(1).toString().equals("dane_surowe")) {
                            DaneSurowe dana = new DaneSurowe();
                            dana.setId(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("id").toString()));
                            dana.setFk_id_punkt_pomiarowy(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_punkt_pomiarowy").toString()));
                            dana.setFk_id_ap(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_ap").toString()));
                            dana.setFk_id_urzadzenie(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_urzadzenie").toString()));
                            dana.setNr_pomiaru(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("nr_pomiaru").toString()));
                            dana.setRssi(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("rssi").toString()));
                            dane.add(dana);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return dane;
    }

    public List<Pomieszczenie> getPomieszczenieList() throws IOException {
        List<Pomieszczenie> pomieszczenia = new ArrayList<Pomieszczenie>();


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListPomieszczenie.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("pomieszczenie").length(); i++) {
                        if (jObj.names().get(1).toString().equals("pomieszczenie")) {
                            Pomieszczenie room = new Pomieszczenie();
                            room.setId(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id").toString()));
                            room.setNazwa(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("nazwa").toString());
                            room.setFk_id_budynek(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("fk_id_budynek").toString()));
                            room.setRozmiar_x(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("rozmiar_x").toString()));
                            room.setRozmiar_y(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("rozmiar_y").toString()));
                            room.setLiczba_pomiarow(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("liczba_pomiarow").toString()));
                            room.setPionowy_odstep_pomiaru(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("pionowy_odstep_pomiaru").toString()));
                            room.setPoziomy_odstep_pomiaru(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("poziomy_odstep_pomiaru").toString()));

                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_n")) {
                                room.setId_N(-1);
                            }
                            else
                            {
                                room.setId_N(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_n").toString()));
                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_s"))
                            {
                                room.setId_S(-1);
                            }
                            else
                            {
                                room.setId_S(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_s").toString()));
                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_e"))
                            {
                                room.setId_E(-1);
                            }
                            else
                            {
                                room.setId_E(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_e").toString()));

                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_w")) {
                                room.setId_W(-1);
                            }
                            else
                            {
                                room.setId_W(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_w").toString()));
                            }
                            pomieszczenia.add(room);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return pomieszczenia;
    }

    public List<DaneZagregowane> getDaneZagregowane() throws IOException {
        List<DaneZagregowane> daneSum = new ArrayList<DaneZagregowane>();


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListDaneZagregowane.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("dane_zagregowane").length(); i++) {
                        if (jObj.names().get(1).toString().equals("dane_zagregowane")) {
                            DaneZagregowane dana = new DaneZagregowane();
                            dana.setId(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("id").toString()));
                            dana.setFk_id_punkt_pomiarowy(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("fk_id_punkt_pomiarowy").toString()));
                            dana.setFk_id_ap(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("fk_id_ap").toString()));
                            dana.setFk_id_urzadzenie(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("fk_id_urzadzenie").toString()));
                            dana.setMin_rssi(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("min_rssi").toString()));
                            dana.setMax_rssi(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("max_rssi").toString()));
                            dana.setMediana(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("mediana").toString()));
                            dana.setDominanta(Integer.parseInt(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("dominanta").toString()));
                            dana.setSrednia_rssi(Float.parseFloat(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("srednia_rssi").toString()));
                            dana.setSredniaPL3s(Float.parseFloat(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("sredniaPL3s").toString()));
                            dana.setSredniaMIN3s(Float.parseFloat(jObj.getJSONArray("dane_zagregowane").getJSONObject(i).get("sredniaMIN3s").toString()));
                            daneSum.add(dana);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return daneSum;
    }


    public List<PunktPomiarowy> getPunktyPomiaroweList() throws IOException {
        List<PunktPomiarowy> punkty = new ArrayList<PunktPomiarowy>();


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListPunktyPomiarowe.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("punkt_pomiarowy").length(); i++) {
                        if (jObj.names().get(1).toString().equals("punkt_pomiarowy")) {
                            PunktPomiarowy pomiar = new PunktPomiarowy();
                            pomiar.setId(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("id").toString()));
                            pomiar.setFk_id_pomieszczenie(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_pomieszczenie").toString()));
                            pomiar.setFk_id_budynek(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_budynek").toString()));
                            pomiar.setPozycja_x(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_x").toString()));
                            pomiar.setPozycja_y(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_y").toString()));
                            pomiar.setGraniczny(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("graniczny").toString()));
                            punkty.add(pomiar);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return punkty;
    }


    public List<Budynek> getAllBudynek() throws IOException {
        List<Budynek> budynki = new ArrayList<Budynek>();


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_ListBudynki.php";
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                Log.d("Name 0", jObj.names().get(0).toString());
                Log.d("Name 1", jObj.names().get(1).toString());
                Log.d("Efekt success", jObj.get("success").toString());

                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("budynek").length(); i++) {
                        if (jObj.names().get(1).toString().equals("budynek")) {
                            Budynek building = new Budynek();
                            building.setId(Integer.parseInt(jObj.getJSONArray("budynek").getJSONObject(i).get("id").toString()));
                            building.setNazwa(jObj.getJSONArray("budynek").getJSONObject(i).get("nazwa").toString());
                            Log.d("building", building.getNazwa());
                            budynki.add(building);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return budynki;
    }


    public Budynek getBudynek(int id_budynek2) throws IOException {
        Budynek budynek = new Budynek();

        String id_budynek = Integer.toString(id_budynek2);
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("id", id_budynek);
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_Budynek.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("budynek").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("budynek"))
                        {
                            budynek.setId(Integer.parseInt(jObj.getJSONArray("budynek").getJSONObject(i).get("id").toString()));
                            budynek.setNazwa(jObj.getJSONArray("budynek").getJSONObject(i).get("nazwa").toString());
                            Log.d("Budynek", budynek.getNazwa());

                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        return budynek;
    }

    public List<DaneSurowe> getDaneSurowe(int id_budynek) throws IOException {
        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();
        String fk_id_budynek = Integer.toString(id_budynek);
        List<PunktPomiarowy> ListId;
        ListId = getPunktyPomiarowebyBudynek(id_budynek);
        for(int m = 0; m < ListId.size(); m++)
        {
            ArrayList params = new ArrayList();
            BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_punkt_pomiarowy", Integer.toString(ListId.get(m).getId()));
            params.add(localBasicNameValuePair);

            String url = "http://bycniebytem.cba.pl/inz_php/get_ListDaneSuroweByPunktPomiarowy.php";
            DefaultHttpClient httpClient = new DefaultHttpClient();
            String paramString = URLEncodedUtils.format(params, "utf-8");
            url += "?" + paramString;
            HttpGet httpGet = new HttpGet(url);

            HttpResponse httpResponse;

            try {
                httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                Log.d("Entity", is.toString());
            } catch (UnsupportedEncodingException e1) {
                Log.e("UnsupportedEncodingException", e1.toString());
                e1.printStackTrace();
            } catch (ClientProtocolException e2) {
                Log.e("ClientProtocolException", e2.toString());
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                Log.e("IllegalStateException", e3.toString());
                e3.printStackTrace();
            } catch (IOException e4) {
                Log.e("IOException", e4.toString());
                e4.printStackTrace();
            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                String line;

                while (!(line = reader.readLine()).equals("db_config.php"))
                {
                }
                String result;
                result = reader.readLine();

                is.close();
                json = result;

            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            try {
                jObj = new JSONObject(json);

                try {
                    if (jObj.get("success").toString().equals("1")) {
                        for (int i = 0; i < jObj.getJSONArray("dane_surowe").length(); i++) {
                            if (jObj.names().get(1).toString().equals("dane_surowe")) {
                                DaneSurowe dana = new DaneSurowe();
                                dana.setId(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("id").toString()));
                                dana.setFk_id_punkt_pomiarowy(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_punkt_pomiarowy").toString()));
                                dana.setFk_id_ap(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_ap").toString()));
                                dana.setFk_id_urzadzenie(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("fk_id_urzadzenie").toString()));
                                dana.setNr_pomiaru(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("nr_pomiaru").toString()));
                                dana.setRssi(Integer.parseInt(jObj.getJSONArray("dane_surowe").getJSONObject(i).get("rssi").toString()));
                                dane.add(dana);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
        }


        return dane;
    }

    public List<Pomieszczenie> getPomieszczenieByBudynek(int id_budynek) throws IOException {
        List<Pomieszczenie> pomieszczenia = new ArrayList<Pomieszczenie>();
        String fk_id_budynek = Integer.toString(id_budynek);

        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_budynek", fk_id_budynek);
        params.add(localBasicNameValuePair);

        String url = "http://bycniebytem.cba.pl/inz_php/get_ListPomieszczenieByBudynek.php";
        DefaultHttpClient httpClient = new DefaultHttpClient();
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try {
            jObj = new JSONObject(json);

            try {
                if (jObj.get("success").toString().equals("1")) {
                    for (int i = 0; i < jObj.getJSONArray("pomieszczenie").length(); i++) {
                        if (jObj.names().get(1).toString().equals("pomieszczenie")) {
                            Pomieszczenie room = new Pomieszczenie();
                            room.setId(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id").toString()));
                            room.setNazwa(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("nazwa").toString());
                            room.setFk_id_budynek(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("fk_id_budynek").toString()));
                            room.setRozmiar_x(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("rozmiar_x").toString()));
                            room.setRozmiar_y(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("rozmiar_y").toString()));
                            room.setLiczba_pomiarow(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("liczba_pomiarow").toString()));
                            room.setPionowy_odstep_pomiaru(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("pionowy_odstep_pomiaru").toString()));
                            room.setPoziomy_odstep_pomiaru(Float.parseFloat(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("poziomy_odstep_pomiaru").toString()));

                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_n")) {
                                room.setId_N(-1);
                            }
                            else
                            {
                                room.setId_N(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_n").toString()));
                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_s"))
                            {
                                room.setId_S(-1);
                            }
                            else
                            {
                                room.setId_S(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_s").toString()));
                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_e"))
                            {
                                room.setId_E(-1);
                            }
                            else
                            {
                                room.setId_E(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_e").toString()));

                            }
                            if(!jObj.getJSONArray("pomieszczenie").getJSONObject(i).has("id_w")) {
                                room.setId_W(-1);
                            }
                            else
                            {
                                room.setId_W(Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id_w").toString()));
                            }
                            pomieszczenia.add(room);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return pomieszczenia;
    }

    public int getMaxIdFrom(String tablename)
    {
       int max = 0;
       ArrayList params = new ArrayList();
       BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("tablename", tablename);
       params.add(localBasicNameValuePair);

       DefaultHttpClient httpClient = new DefaultHttpClient();
       String url = "http://bycniebytem.cba.pl/inz_php/get_MaxIdFrom.php";
       String paramString = URLEncodedUtils.format(params, "utf-8");
       url += "?" + paramString;
       HttpGet httpGet = new HttpGet(url);

       HttpResponse httpResponse;

            try {
                httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                Log.d("Entity", is.toString());
            } catch (UnsupportedEncodingException e1) {
                Log.e("UnsupportedEncodingException", e1.toString());
                e1.printStackTrace();
            } catch (ClientProtocolException e2) {
                Log.e("ClientProtocolException", e2.toString());
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                Log.e("IllegalStateException", e3.toString());
                e3.printStackTrace();
            } catch (IOException e4) {
                Log.e("IOException", e4.toString());
                e4.printStackTrace();
            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                String line;

                while (!(line = reader.readLine()).equals("db_config.php"))
                {
                }
                String result;
                result = reader.readLine();

                is.close();
                json = result;

            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            try
            {
                jObj = new JSONObject(json);

                try
                {
                    if (jObj.get("success").toString().equals("1"))
                    {
                        for (int i = 0; i < jObj.getJSONArray("max").length(); i++)
                        {
                            if (jObj.names().get(1).toString().equals("max"))
                            {
                                if(jObj.getJSONArray("max").getJSONObject(i).get("MAX(id)").equals(null))
                                {
                                    max = 0;
                                }
                                else
                                {
                                    max = Integer.parseInt(jObj.getJSONArray("max").getJSONObject(i).get("MAX(id)").toString());
                                }
                                Log.d("MAX", Integer.toString(max));
                            }
                        }

                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            catch (JSONException e)
            {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }


        return max;
    }


    public int getIdPomieszczenieByName(int id_budynek , String roomname)
    {
        int id = 0;
        ArrayList params = new ArrayList();
        BasicNameValuePair value = new BasicNameValuePair("nazwa", "\'" + roomname + "\'");
        params.add(value);
        value = new BasicNameValuePair("fk_id_budynek", Integer.toString(id_budynek));
        params.add(value);


        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_IdPomieszczenieByName.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("pomieszczenie").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("pomieszczenie"))
                        {
                            if(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id").equals(null))
                            {
                                id = 0;
                            }
                            else
                            {
                                id = Integer.parseInt(jObj.getJSONArray("pomieszczenie").getJSONObject(i).get("id").toString());
                                Log.d("pomieszczenie", Integer.toString(id));
                            }
                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        return id ;
    }



    public List<PunktPomiarowy> getPunktyPomiarowebyPomieszczenie(int id_pomieszczenie) throws IOException {
        List<PunktPomiarowy> punkty = new ArrayList<PunktPomiarowy>();
        String fk_id_pomieszczenie = Integer.toString(id_pomieszczenie);

        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_pomieszczenie", fk_id_pomieszczenie);
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String paramString = URLEncodedUtils.format(params, "utf-8");

        String url = "http://bycniebytem.cba.pl/inz_php/get_PunktyPomiarowebyPomieszczenie.php";
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("punkt_pomiarowy").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("punkt_pomiarowy"))
                        {
                            PunktPomiarowy pomiar = new PunktPomiarowy();
                            pomiar.setId(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("id").toString()));
                            pomiar.setFk_id_pomieszczenie(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_pomieszczenie").toString()));
                            pomiar.setFk_id_budynek(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("fk_id_budynek").toString()));
                            pomiar.setPozycja_x(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_x").toString()));
                            pomiar.setPozycja_y(Float.parseFloat(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("pozycja_y").toString()));
                            pomiar.setGraniczny(Integer.parseInt(jObj.getJSONArray("punkt_pomiarowy").getJSONObject(i).get("graniczny").toString()));
                            punkty.add(pomiar);
                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return punkty;
    }

    public int getIdApByMac(String mac)
    {
        int id = 0;
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("mac","\'"+ mac +"\'");
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_IdAPByMac.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("accesspoint").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("accesspoint"))
                        {
                            if(jObj.getJSONArray("accesspoint").getJSONObject(i).get("id").equals(null))
                            {
                                id = 0;
                            }
                            else
                            {
                                id = Integer.parseInt(jObj.getJSONArray("accesspoint").getJSONObject(i).get("id").toString());
                                Log.d("AP", Integer.toString(id));
                            }

                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        return id ;

    }

    public int getIdUrzadzeniebyMac(String mac)
    {
        int id = 0;
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("mac", "\'"+ mac +"\'");
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_IdUrzadzeniebyMac.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("urzadzenie").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("urzadzenie"))
                        {
                            if(jObj.getJSONArray("urzadzenie").getJSONObject(i).get("id").equals(null))
                            {
                                id = 0;
                            }
                            else
                            {
                                id = Integer.parseInt(jObj.getJSONArray("urzadzenie").getJSONObject(i).get("id").toString());
                                Log.d("Urzadzenie", Integer.toString(id));
                            }

                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        return id ;
    }

    public int getIdByBuildingName(String buildingname)
    {
        int id = 0;
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("nazwa", "\'" + buildingname + "\'");
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/get_IdByBuildingName.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;

            while (!(line = reader.readLine()).equals("db_config.php"))
            {
            }
            String result;
            result = reader.readLine();

            is.close();
            json = result;

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        try
        {
            jObj = new JSONObject(json);

            try
            {
                if (jObj.get("success").toString().equals("1"))
                {
                    for (int i = 0; i < jObj.getJSONArray("budynek").length(); i++)
                    {
                        if (jObj.names().get(1).toString().equals("budynek"))
                        {
                            if(jObj.getJSONArray("budynek").getJSONObject(i).get("id").equals(null))
                            {
                                id = 0;
                            }
                            else
                            {
                                id = Integer.parseInt(jObj.getJSONArray("budynek").getJSONObject(i).get("id").toString());
                                Log.d("Budynek", Integer.toString(id));
                            }
                        }
                    }

                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (JSONException e)
        {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }


        return id ;
    }

    public void deletePunktyPomiaroweByFkIdPomieszczenie(int fk_id_pomieszczenie)
    {
        ArrayList params = new ArrayList();
        BasicNameValuePair localBasicNameValuePair = new BasicNameValuePair("fk_id_pomieszczenie", Integer.toString(fk_id_pomieszczenie));
        params.add(localBasicNameValuePair);

        DefaultHttpClient httpClient = new DefaultHttpClient();
        String url = "http://bycniebytem.cba.pl/inz_php/delete_PunktPomiarowyIdPomieszczenie.php";
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" + paramString;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
            Log.d("Entity", is.toString());
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingException", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolException", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateException", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOException", e4.toString());
            e4.printStackTrace();
        }

    }
}
