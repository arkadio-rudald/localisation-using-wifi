package com.example.arkadio.wifimapping;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.WifiManager;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    EditText inputBuildingName;
    Button b_next;
    Button b_chooseBuilding;

    //Stworzenie buttona, który umożliwi nam polaczenie z internetem i zczytanie budynków

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        inputBuildingName = (EditText) findViewById(R.id.editText);
        b_chooseBuilding = (Button) findViewById(R.id.button5);
        b_chooseBuilding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wyświetlenie budynków z bazy SQLITE
            }
        });

        b_next = (Button) findViewById(R.id.button);

        b_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String budynekName = inputBuildingName.getText().toString();
                if(budynekName.equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Alert Dialog")
                            .setMessage("Uzupełnij wszystkie pola")
                            .setCancelable(false)
                            .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else
                {
                    Budynek budynek = new Budynek();
                    budynek.setNazwa(budynekName);

                    SqLiteDatabaseCRUD database = new SqLiteDatabaseCRUD(getApplicationContext());
                    try {
                        database.createDataBase();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }


                    //Sprawdzenie czy budynek istnieje w bazie wewnetrznej
                    List<Budynek> testowaLista;
                    testowaLista = database.getAllBudynek();
                    boolean exist = false;
                    for(int i = 0; i < testowaLista.size(); i++)
                    {
                        if(testowaLista.get(i).getNazwa().equals(budynekName))
                        {
                            budynek.setId(testowaLista.get(i).getId());
                            exist = true;

                            break;
                        }
                    }

                    if(!exist)
                    {
                        int id_budynek = database.getMaxIdFromTable("BUDYNEK");
                        id_budynek++;
                        budynek.setId(id_budynek);
                        database.createFieldBudynek(budynek);
                    }
                    database.closeDB();

                    Intent intent = new Intent(getApplicationContext(), RoomActivity.class);
                    intent.putExtra("id_budynek", budynek.getId());
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
