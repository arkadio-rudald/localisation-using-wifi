package com.example.arkadio.wifimapping;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PomiarActivity extends ActionBarActivity {

    private WifiManager wifiManager;
    private EditText inputCoorX;
    private EditText inputCoorY;
    private Button b_pomiar;
    private Button b_pomin_pomiar;
    private Button b_eksport;
    private Button b_koniec;
    private RadioGroup radioMethod;
    private RadioButton radioMethodButton;
    private TextView inputText, inputText2, inputX, inputY;
    private int selectedOption;

    int id_pomieszczenie, id_budynek, id;
    private SqLiteDatabaseCRUD database;
    private Urzadzenie device;


    private List<ScanResult> lista;
    private List<String> ssidList;
    private List<String> bssidList;
    private List<Integer> rssiList;
    private List<Integer> frequencyList;
    private PunktPomiarowy pkt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pomiar);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        inputCoorX = (EditText) findViewById(R.id.x);
        inputCoorY = (EditText) findViewById(R.id.y);
        radioMethod = (RadioGroup) findViewById(R.id.radioMethod);
        inputText = (TextView) findViewById(R.id.textView14);
        inputText2 = (TextView) findViewById(R.id.textView13);
        inputX = (TextView) findViewById(R.id.textView15);
        inputY = (TextView) findViewById(R.id.textView16);

        inputCoorX.setVisibility(View.GONE);
        inputCoorY.setVisibility(View.GONE);
        inputText2.setVisibility(View.GONE);
        inputX.setVisibility(View.GONE);
        inputY.setVisibility(View.GONE);

        b_pomiar = ((Button)findViewById(R.id.button2));
        b_pomiar.setOnClickListener(pomiar);

        b_koniec = ((Button)findViewById(R.id.button3));
        b_koniec.setOnClickListener(koniec);

        b_eksport = (Button)findViewById(R.id.button7);
        b_eksport.setOnClickListener(eksport);

        b_pomin_pomiar = ((Button)findViewById(R.id.button6));
        b_pomin_pomiar.setOnClickListener(pomin);

         selectedOption = radioMethod.getCheckedRadioButtonId();
        radioMethodButton = (RadioButton) findViewById(selectedOption);

        radioMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {

            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if(radioMethodButton.isChecked())
                {
                    pkt = database.getPunktPomiarowybyId(id);
                    inputCoorX.setVisibility(View.GONE);
                    inputCoorY.setVisibility(View.GONE);
                    inputText2.setVisibility(View.GONE);
                    inputX.setVisibility(View.GONE);
                    inputY.setVisibility(View.GONE);
                    selectedOption = radioMethod.getCheckedRadioButtonId();
                    inputText.setVisibility(View.VISIBLE);
                    inputText.setText("Idź do punktu  [" + pkt.getPozycja_x() + "  |  " +  pkt.getPozycja_y()+"]");
                    Log.d("Radio Auto: ", "VISIBLE");
                }
                else
                {
                    Float coorX, coorY;
                    inputText.setVisibility(View.GONE);
                    inputText2.setVisibility(View.VISIBLE);
                    inputCoorX.setVisibility(View.VISIBLE);
                    inputCoorY.setVisibility(View.VISIBLE);
                    inputX.setVisibility(View.VISIBLE);
                    inputY.setVisibility(View.VISIBLE);
                    selectedOption = radioMethod.getCheckedRadioButtonId();
                    Log.d("Radio MANUAL: ", "INVISIBLE");
                }
            }
        });

        Intent intent = getIntent();
        if (null != intent)
        {
            //id budynku oraz pomieszczenia w ktorym mierzymy

            id_budynek = intent.getIntExtra("id_budynek", 0);
            id_pomieszczenie = intent.getIntExtra("id_pomieszczenie", 0);
        }

        wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        database = new SqLiteDatabaseCRUD(getApplicationContext());

        WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();


        device = database.getUrzadzenieByMac(address);

        if(device == null)
        {
            device = new Urzadzenie();
            device.setId(1);
            device.setMac(address);
            database.createFieldUrzadzenie(device);
        }

        id = database.getMinIdFromPunktPomiarowyByBudynek(id_budynek,id_pomieszczenie);
        pkt = database.getPunktPomiarowybyId(id);
        inputText.setText("Idź do punktu  [" + pkt.getPozycja_x() + "  |  " +  pkt.getPozycja_y()+"]");

        AlertDialog.Builder builder = new AlertDialog.Builder(PomiarActivity.this);
        builder.setTitle("Alert Dialog")
                .setMessage("Pamiętaj, że pomiar zaczynamy w NW (PnZach, połączenie z internetem NIE jest wymagane)")
                .setCancelable(false)
                .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();


    }

    public View.OnClickListener pomin = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {


            id++;
        }
    };

    public View.OnClickListener eksport = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            if(!wifiManager.isWifiEnabled())
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(PomiarActivity.this);
                builder.setTitle("Alert Dialog")
                        .setMessage("Wymagane połączenie z internetem")
                        .setCancelable(false)
                        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            else {

                final Thread exDatabase1 = new Thread(eksport_danych, "external_database1");
                exDatabase1.start();
                try {
                    exDatabase1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    public View.OnClickListener pomiar = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

            //pobranie ostatniego id AP;
            //sprawdzenie który id jest większy zewn czy wewn
            String testowo = (String) radioMethodButton.getText();

            Log.d("testowo", testowo);
            if(testowo.equals("Manualnie")) {
                Float coorX, coorY;

                coorX = Float.parseFloat(inputCoorX.getText().toString());
                coorY = Float.parseFloat(inputCoorY.getText().toString());
                pkt = database.getPunktPomiarowyByPosition(id_pomieszczenie, coorX, coorY);
            }


            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            int max_id_ap_internal = database.getMaxIdFromTable("AP");
            int max_id_dane_surowe_internal = database.getMaxIdFromTable("DANE_SUROWE");

            for(int i = 0; i < 100; i++)
            {
                wifiManager.startScan();
                lista = wifiManager.getScanResults();
                ssidList = new ArrayList<String>(lista.size());
                bssidList = new ArrayList<String>(lista.size());
                rssiList = new ArrayList<Integer>(lista.size());
                frequencyList = new ArrayList<Integer>(lista.size());

                for (ScanResult result : lista)
                {
                    ssidList.add(result.SSID);
                    bssidList.add(result.BSSID);
                    rssiList.add(result.level);
                    frequencyList.add(result.frequency);
                }

                AP router = new AP();
                DaneSurowe dana = new DaneSurowe();


                for(int j = 0; j < bssidList.size(); j++)
                {
                    //najpierw wpisujemy AP
                    //sprawdzenie czy już istnieje podany AP
                    AP test;
                    test = database.getAPbyMac(bssidList.get(j));

                    if(test == null)
                    {
                        max_id_ap_internal++;
                        router.setId(max_id_ap_internal);
                        router.setSsid(ssidList.get(j));
                        router.setMac(bssidList.get(j));
                        router.setFrequency(frequencyList.get(j));
                        database.createFieldAP(router);
                        dana.setFk_id_ap(router.getId());
                    }
                    else
                    {
                        dana.setFk_id_ap(test.getId());
                    }

                    max_id_dane_surowe_internal++;
                    dana.setId(max_id_dane_surowe_internal);
                    dana.setFk_id_urzadzenie(device.getId());
                    dana.setFk_id_punkt_pomiarowy(pkt.getId());
                    dana.setNr_pomiaru(i+1);
                    dana.setRssi(rssiList.get(j));
                    database.createFieldDaneSurowe(dana);
                }
            }


            List<Integer> apList = database.getAPFromDaneSuroweByPunktPom(pkt.getId());
            List<Integer> rssiList = null;

            float srednia;
            DaneZagregowane dana = new DaneZagregowane();
            dana.setFk_id_punkt_pomiarowy(pkt.getId());
            dana.setFk_id_urzadzenie(device.getId());
            int max_id_dane_zagregowane = database.getMaxIdFromTable("DANE_ZAGREGOWANE");

            for(int i = 0; i < apList.size(); i++)
            {
                rssiList = (database.getRssiFromDaneSuroweByPunktPomAP(pkt.getId(), apList.get(i)));
                rssiList = sortingList(rssiList);
                max_id_dane_zagregowane++;
                dana.setId(max_id_dane_zagregowane);

                dana.setFk_id_ap(apList.get(i));
                dana.setMax_rssi(Collections.max(rssiList));
                dana.setMin_rssi(Collections.min(rssiList));
                srednia = calculateAverage(rssiList);
                dana.setSrednia_rssi(calculateAverage(rssiList));
                dana.setMediana(calculateMedian(rssiList));
                dana.setDominanta(calculateMode(rssiList));
                dana.setSredniaPL3s((float) (srednia + 3*Math.sqrt(calculateStdDev(srednia, rssiList))));
                dana.setSredniaMIN3s((float) (srednia - 3*Math.sqrt(calculateStdDev(srednia, rssiList))));
                database.createFieldDaneZagregowane(dana);
            }
            id++;
            pkt = database.getPunktPomiarowybyId(id);
            inputText.setText("Idź do punktu  [" + pkt.getPozycja_x() + "  |  " +  pkt.getPozycja_y()+"]");


            try
		     {
		         Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		         Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
		         r.play();
		     }
		     catch (Exception e)
		     {
		         e.printStackTrace();
		     }

            database.closeDB();
            /*
            odpalić dźwięk, set text i gra
             */
        }
    };

    private double calculateVariance(float srednia, List<Integer> rssiList)
    {
        double mean = srednia;
        double temp = 0;
        for(double a :rssiList)
            temp += (mean-a)*(mean-a);
        return temp/rssiList.size();
    }

    private double calculateStdDev(float srednia, List<Integer> rssiList)
    {
        return Math.sqrt(calculateVariance(srednia, rssiList));
    }

    private List<Integer> sortingList(List<Integer> rssiList)
    {
        int temp;
        for (int i = 0; i<rssiList.size()-1; i++)
        {
            for (int j=0; j<rssiList.size()-i-1; j++)
            {
                if (rssiList.get(j) > rssiList.get(j+1))
                {
                    temp = rssiList.get(j+1);
                    rssiList.set(j+1, rssiList.get(j));
                    rssiList.set(j, temp);
                }
            }
        }

        return rssiList;
    }

    private float calculateAverage(List <Integer> rssiList) {
        Integer sum = 0;
        if(!rssiList.isEmpty()) {
            for (Integer mark : rssiList) {
                sum += mark;
            }
            return sum.floatValue() / rssiList.size();
        }
        return sum;
    }

    private int calculateMedian(List<Integer> rssiList) {

        int middle = rssiList.size()/2;
        if (rssiList.size()%2 == 1) {
            return rssiList.get(middle);
        } else {
            return (rssiList.get(middle-1) + rssiList.get(middle)) / 2;
        }
    }

    private int calculateMode(List<Integer> rssiList) {
        int maxValue = 0, maxCount = 0;

        for (int i = 0; i < rssiList.size(); ++i) {
            int count = 0;
            for (int j = 0; j < rssiList.size(); ++j) {
                if (rssiList.get(j) == rssiList.get(i)) ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = rssiList.get(i);
            }
        }

        return maxValue;
    }




    public View.OnClickListener koniec = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!wifiManager.isWifiEnabled())
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(PomiarActivity.this);
                builder.setTitle("Alert Dialog")
                        .setMessage("Wymagane połączenie z internetem")
                        .setCancelable(false)
                        .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
            else {

                final Thread exDatabase1 = new Thread(aktualizuj_baze, "external_database1");
                exDatabase1.start();
                try {
                    exDatabase1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }



        }
    };

            /*Wszystkie dane znajdują się w SqLite

        Pobieramy listy wszystkich elementów.

        1. Sprawdzenie czy istnieje budynek o zadanej nazwie
         a) TAK, pobieramy jego id i wpisujemy do pomieszczenia jak fk_id_budynek,
         b) NIE, pobieramy max(id) z tabeli BUDYNEK dodajemy 1 i uzupełniamy fk_id_budynek w pomieszczeniu,
        2. Sprawdzenie czy istnieje pomieszczenie o zadanej nazwie
          a) TAK, pobieramy jego id i wpisujemy do punktów pomiarowych jak fk_id_pomieszczenie
          b) NIE, pobieramy max(id) z tabeli POMIESZCZENIE dodajemy 1 i uzupełniamy fk_id_pomieszczenie w Punktach pomiarowych
        3. Pobranie max(id) punktow pomiarowych i dodanie jeden, uzupełnianie wszystkich fk_id


        */

    //NIE DOKONCZONE ID DANYCH
    Runnable eksport_danych = new Runnable()
    {
        ExternalDatabase exConnection = new ExternalDatabase();


        //sprawdzić czy istnieje budynek, pomieszczenie, punkty pomiarowe, ap, urzadzenie
        int max_id_budynek;
        int max_id_pomieszczenie;
        int max_id_ap;
        int max_id_punkt_pomiarowy;
        int max_id_urzadzenie;
        int max_id_dane_surowe;
        int max_id_dane_zagregowane;

        int id_b, id_pom, id_urz, id_ap;
        @Override
        public void run()
        {
            Budynek budynek;
            database = new SqLiteDatabaseCRUD(getApplicationContext());
            budynek = database.getBudynek(id_budynek);
            Pomieszczenie room = null;

            room = database.getPomieszczenieById(id_pomieszczenie);

            id_urz = exConnection.getIdUrzadzeniebyMac(device.getMac());
            if(id_urz == 0)
            {
                max_id_urzadzenie = exConnection.getMaxIdFrom("URZADZENIE");
                max_id_urzadzenie++;
                database.updateUrzadzenie(device.getId(),max_id_urzadzenie);
                device.setId(max_id_urzadzenie);
            }
            else
            {
                database.updateUrzadzenie(device.getId(),id_urz);
                device.setId(id_urz);
            }

            //pobranie listy AP
            List<AP> aplist = database.getAPList();
            List<AP> aplistex = null;
            try {
                aplistex = exConnection.getAPbyBudynek(id_budynek);
            } catch (IOException e) {
                e.printStackTrace();
            }

            max_id_ap = exConnection.getMaxIdFrom("AP") + database.getMaxIdFromTable("AP");
            //sprawdzamy tylko AP będące w 1 budynku

            List<AP> add_aps = new ArrayList<AP>();
            List<Integer> id_list_old = new ArrayList<Integer>();
            List<Integer> id_list = new ArrayList<Integer>();
            if(aplistex.size() == 0)
            {
                for(int j = aplist.size()-1; j > -1; j--)
                {
                    List<DaneSurowe> daneSuroweList = database.getDaneSuroweByAP(aplist.get(j).getId());
                    List<DaneZagregowane> daneZagregowaneList = database.getDaneZagregowaneByAP(aplist.get(j).getId());

                    aplist.get(j).setId(max_id_ap);
                    for(int k = 0; k < daneSuroweList.size(); k++)
                    {
                        database.updateDaneSurowe(daneSuroweList.get(k).getId(), daneSuroweList.get(k).getId(), max_id_ap, daneSuroweList.get(k).getFk_id_punkt_pomiarowy(), device.getId() );
                    }
                    for(int k = 0; k < daneZagregowaneList.size(); k++)
                    {
                        database.updateDaneZagregowane(daneZagregowaneList.get(k).getId(), daneZagregowaneList.get(k).getId(), max_id_ap, daneZagregowaneList.get(k).getFk_id_punkt_pomiarowy(), device.getId() );
                    }
                    database.updateAP(aplist.get(j).getId(), max_id_ap);
                    max_id_ap--;
                }
            }
            else
            {
                //usuniecie w celu pominiecia wyjatku nadpisywania ID
                database.deleteAPs();
                int k = 0;
                for(int j = aplist.size()-1; j >= 0; j--)
                {
                    for(int i = 0; i < aplistex.size(); i++)
                    {
                        if(aplistex.get(i).getMac().equals(aplist.get(j).getMac()))
                        {
                            break;
                        }
                        else if(i == aplistex.size()-1 && !aplistex.get(i).getMac().equals(aplist.get(j).getMac()))
                        {
                            k++;
                            id_list_old.add(aplist.get(j).getId());
                        }
                    }
                }


                max_id_ap = exConnection.getMaxIdFrom("AP");

                for(int j = id_list_old.size()-1; j >= 0; j--)
                {
                    for(int i = 0; i < aplist.size(); i++)
                    {
                        if(aplist.get(i).getId() == id_list_old.get(j))
                        {
                            max_id_ap++;
                            AP pomoc = new AP();
                            pomoc.setId(max_id_ap);
                            pomoc.setSsid(aplist.get(i).getSsid());
                            pomoc.setMac(aplist.get(i).getMac());
                            pomoc.setFrequency(aplist.get(i).getFrequency());
                            database.createFieldAP(pomoc);
                            List<DaneSurowe> daneSuroweList = database.getDaneSuroweByAP(id_list_old.get(j));
                            List<DaneZagregowane> daneZagregowaneList = database.getDaneZagregowaneByAP(id_list_old.get(j));

                            for(int h = 0; h < daneSuroweList.size(); h++)
                            {
                                database.updateDaneSurowe(daneSuroweList.get(h).getId(), daneSuroweList.get(h).getId(), max_id_ap, daneSuroweList.get(h).getFk_id_punkt_pomiarowy(), device.getId() );
                            }
                            for(int h = 0; h < daneZagregowaneList.size(); h++)
                            {
                                database.updateDaneZagregowane(daneZagregowaneList.get(h).getId(), daneZagregowaneList.get(h).getId(), max_id_ap, daneZagregowaneList.get(h).getFk_id_punkt_pomiarowy(), device.getId() );
                            }

                            break;
                        }
                    }
                }

                for(int j = aplist.size()-1; j >= 0; j--)
                {
                    for(int i = 0; i < aplistex.size(); i++)
                    {
                        if(aplistex.get(i).getMac().equals(aplist.get(j).getMac()))
                        {
                            List<DaneSurowe> daneSuroweList = database.getDaneSuroweByAP(aplist.get(j).getId());
                            List<DaneZagregowane> daneZagregowaneList = database.getDaneZagregowaneByAP(aplist.get(j).getId());

                            for(int m = 0; m < daneSuroweList.size(); m++)
                            {
                                database.updateDaneSurowe(daneSuroweList.get(m).getId(), daneSuroweList.get(m).getId(), aplistex.get(i).getId(), daneSuroweList.get(m).getFk_id_punkt_pomiarowy(), device.getId() );
                            }
                            for(int m = 0; m < daneZagregowaneList.size(); m++)
                            {
                                database.updateDaneZagregowane(daneZagregowaneList.get(m).getId(), daneZagregowaneList.get(m).getId(), aplistex.get(i).getId(), daneZagregowaneList.get(m).getFk_id_punkt_pomiarowy(), device.getId() );
                            }
                            database.createFieldAP(aplistex.get(i));
                            break;
                        }
                    }
                }
            }




            List<PunktPomiarowy> punkty = database.getPunktPomiarowyByPomieszczenie(id_pomieszczenie);

            List<PunktPomiarowy> punkty_kopia = punkty;

            id_b = exConnection.getIdByBuildingName(budynek.getNazwa());
            if(id_b == 0)
            {
                //id_max_id_budynek
                max_id_budynek = exConnection.getMaxIdFrom("BUDYNEK");
                max_id_budynek++;
                database.updateBudynek(budynek.getId(), max_id_budynek);
                budynek.setId(max_id_budynek);

                max_id_pomieszczenie = exConnection.getMaxIdFrom("POMIESZCZENIE");
                max_id_pomieszczenie++;
                database.updatePomieszczenie(room.getId(),max_id_pomieszczenie,max_id_budynek);
                room.setId(max_id_pomieszczenie);
                room.setFk_id_budynek(max_id_budynek);

                database.deletePunktyPomiarowe();
                max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                for(int i = 0; i < punkty.size(); i--)
                {
                    max_id_punkt_pomiarowy++;
                    punkty.get(i).setId(max_id_punkt_pomiarowy);
                    punkty.get(i).setFk_id_pomieszczenie(max_id_pomieszczenie);
                    punkty.get(i).setFk_id_budynek(max_id_budynek);
                    database.createFieldPunktPomiarowy(punkty.get(i));
                }
            }
            else
            {
                //jezeli pomieszczenie nie istnialo dla tego budynku
                id_pom = exConnection.getIdPomieszczenieByName(id_b ,room.getNazwa());
                if(id_pom == 0)
                {
                    //max_id_pomieszczenie
                    max_id_pomieszczenie = exConnection.getMaxIdFrom("POMIESZCZENIE");
                    max_id_pomieszczenie++;
                    database.updateBudynek(id_b, max_id_budynek);
                    room.setId(max_id_pomieszczenie);
                    room.setFk_id_budynek(id_b);

                    database.deletePunktyPomiarowe();
                    max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                    for(int i = 0; i < punkty.size(); i--)
                    {
                        max_id_punkt_pomiarowy++;
                        punkty.get(i).setId(max_id_punkt_pomiarowy);
                        punkty.get(i).setFk_id_pomieszczenie(max_id_pomieszczenie);
                        punkty.get(i).setFk_id_budynek(id_b);
                        database.createFieldPunktPomiarowy(punkty.get(i));
                    }

                }
                else //jest pomieszczenie
                {
                    List<PunktPomiarowy> punktyEx = null;
                    try
                    {
                        punktyEx = exConnection.getPunktyPomiarowebyPomieszczenie(id_pom);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    if(aplistex.size() == 0)
                    {
                        //upload ap
                    }
                    else
                    {
                        //sprawdzenie czy punkty już istnieją
                        database.deletePunktyPomiarowe();
                        max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                        for(int i = 0; i < punkty.size(); i--)
                        {
                            max_id_punkt_pomiarowy++;
                            punkty.get(i).setId(max_id_punkt_pomiarowy);
                            punkty.get(i).setFk_id_pomieszczenie(id_pom);
                            punkty.get(i).setFk_id_budynek(id_b);
                            database.createFieldPunktPomiarowy(punkty.get(i));
                        }
                    }
                }
            }
        }
    };



    Runnable aktualizuj_baze = new Runnable()
    {
        ExternalDatabase exConnection = new ExternalDatabase();


        //sprawdzić czy istnieje budynek, pomieszczenie, punkty pomiarowe, ap, urzadzenie
        int max_id_budynek;
        int max_id_pomieszczenie;
        int max_id_ap;
        int max_id_punkt_pomiarowy;
        int max_id_urzadzenie;
        int max_id_dane_surowe;
        int max_id_dane_zagregowane;

        int id_b, id_pom, id_urz, id_ap;
        @Override
        public void run()
        {
            Budynek budynek;
            database = new SqLiteDatabaseCRUD(getApplicationContext());
            budynek = database.getBudynek(id_budynek);
            Pomieszczenie room = null;

            room = database.getPomieszczenieById(id_pomieszczenie);

            id_urz = exConnection.getIdUrzadzeniebyMac(device.getMac());
            if(id_urz == 0)
            {
                max_id_urzadzenie = exConnection.getMaxIdFrom("URZADZENIE");
                max_id_urzadzenie++;
                device.setId(max_id_urzadzenie);
                //update DEVICE
                exConnection.createFieldUrzadzenie(device);
            }
            else
            {
                device.setId(id_urz);
                //update ID
            }


            //pobranie listy AP
            List<AP> aplist = database.getAPList();
            List<AP> aplistex = null;
            try {
                aplistex = exConnection.getAPbyBudynek(id_budynek);
            } catch (IOException e) {
                e.printStackTrace();
            }


            max_id_ap = exConnection.getMaxIdFrom("AP");
            //sprawdzamy tylko AP będące w 1 budynku
            AP apUpload = new AP();

            if(aplistex.size() == 0)
            {
                for(int j = 0; j < aplist.size(); j++)
                {
                    max_id_ap++;
                    aplist.get(j).setId(max_id_ap);
                    //update AP id
                    exConnection.createFieldAP(aplist.get(j));
                }
            }
            else
            {
                for(int i = 0; i < aplistex.size(); i++)
                {
                    for(int j = 0; j < aplist.size(); j++)
                    {
                        if(aplistex.get(i).getMac().equals(aplist.get(j).getMac()))
                        {
                            break;
                        }
                        else if(j == aplist.size()-1 && !aplistex.get(i).getMac().equals(aplist.get(j).getMac()))
                        {
                            max_id_ap++;
                            aplist.get(j).setId(max_id_ap);
                            //update AP id
                            exConnection.createFieldAP(aplist.get(j));
                            //upload aplist.get(j);

                        }
                    }
                }
            }



            List<PunktPomiarowy> punkty = database.getPunktPomiarowyByPomieszczenie(id_pomieszczenie);

            List<PunktPomiarowy> punkty_kopia = punkty;

            id_b = exConnection.getIdByBuildingName(budynek.getNazwa());
            if(id_b == 0)
            {
                //id_max_id_budynek
                max_id_budynek = exConnection.getMaxIdFrom("BUDYNEK");
                max_id_budynek++;
                budynek.setId(max_id_budynek);
                //update BUDYNEK
                exConnection.createFieldBudynek(budynek);
                //upload budynku

                max_id_pomieszczenie = exConnection.getMaxIdFrom("POMIESZCZENIE");
                max_id_pomieszczenie++;
                room.setId(max_id_pomieszczenie);
                room.setFk_id_budynek(max_id_budynek);
                //update POMIESZCZENIE
                exConnection.createFieldPomieszczenie(room);
                //upload room


                //punkty pomiarowe zależą od pomieszczenia
                //    fk_id_pomieszczenie int NOT NULL,
                //fk_id_budynek int NOT NULL,

                max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                for(int i = 0; i < punkty.size(); i++)
                {
                    max_id_punkt_pomiarowy++;
                    punkty.get(i).setId(max_id_punkt_pomiarowy);
                    punkty.get(i).setFk_id_budynek(max_id_budynek);
                    punkty.get(i).setFk_id_pomieszczenie(max_id_pomieszczenie);
                    //update PUNKTY
                    exConnection.createFieldPunktPomiarowy(punkty.get(i));
                    //upload punkty
                }
            }
            else
            {
                //jezeli pomieszczenie nie istnialo dla tego budynku
                id_pom = exConnection.getIdPomieszczenieByName(id_b ,room.getNazwa());
                if(id_pom == 0)
                {
                    //max_id_pomieszczenie
                    max_id_pomieszczenie = exConnection.getMaxIdFrom("POMIESZCZENIE");
                    max_id_pomieszczenie++;
                    room.setId(max_id_pomieszczenie);
                    room.setFk_id_budynek(id_b);
                    exConnection.createFieldPomieszczenie(room);
                    //upload room

                    max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                    for(int i = 0; i < punkty.size(); i++)
                    {
                        max_id_punkt_pomiarowy++;
                        punkty.get(i).setId(max_id_punkt_pomiarowy);
                        punkty.get(i).setFk_id_budynek(max_id_budynek);
                        punkty.get(i).setFk_id_pomieszczenie(max_id_pomieszczenie);
                        exConnection.createFieldPunktPomiarowy(punkty.get(i));
                        //upload punkty
                    }

                }
                else //jest pomieszczenie
                {
                    List<PunktPomiarowy> punktyEx = null;
                    try
                    {
                        punktyEx = exConnection.getPunktyPomiarowebyPomieszczenie(id_pom);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    if(aplistex.size() == 0)
                    {
                        //upload ap
                    }
                    else
                    {
                        max_id_punkt_pomiarowy = exConnection.getMaxIdFrom("PUNKT_POMIAROWY");
                        for(int i = 0; i < punktyEx.size(); i++)
                        {
                            for(int j = 0; j < punkty.size(); j++)
                            {
                                if(punktyEx.get(i).getPozycja_x() == punkty.get(j).getPozycja_x() && punktyEx.get(i).getPozycja_y() == punkty.get(j).getPozycja_y())
                                {
                                    break;
                                }
                                else if(j == punkty.size()-1 && !(punktyEx.get(i).getPozycja_x() == punkty.get(j).getPozycja_x() && punktyEx.get(i).getPozycja_y() == punkty.get(j).getPozycja_y()))
                                {
                                    max_id_punkt_pomiarowy++;
                                    punkty.get(j).setId(max_id_punkt_pomiarowy);
                                    punkty.get(i).setFk_id_budynek(id_b);
                                    punkty.get(i).setFk_id_pomieszczenie(id_pom);
                                    exConnection.createFieldPunktPomiarowy(punkty.get(i));
                                    //upload
                                }
                            }
                        }
                    }
                }
            }

            max_id_dane_surowe = exConnection.getMaxIdFrom("DANE_SUROWE");
            max_id_dane_zagregowane = exConnection.getMaxIdFrom("DANE_ZAGREGOWANE");

            for(int i = 0; i < punkty_kopia.size(); i++)
            {
                List<DaneSurowe> daneSuroweList = database.getDaneSuroweByPunktPomiarowy(punkty_kopia.get(i).getId());
                List<DaneZagregowane> daneZagregowaneList = database.getDaneZagregowaneByPunktPomiarowy(punkty_kopia.get(i).getId());


                for(int j = 0; j < daneZagregowaneList.size(); j++)
                {
                    max_id_dane_zagregowane++;

                    AP apo = database.getAPbyId(daneZagregowaneList.get(j).getFk_id_ap());
                    String mac = apo.getMac();
                    int ap = exConnection.getIdApByMac(mac);

                    daneZagregowaneList.get(j).setId(max_id_dane_zagregowane);
                    daneZagregowaneList.get(j).setFk_id_punkt_pomiarowy(punkty.get(i).getId());
                    daneZagregowaneList.get(j).setFk_id_ap(ap);
                    daneZagregowaneList.get(j).setFk_id_urzadzenie(device.getId());
                    exConnection.createFieldDaneZagregowane(daneZagregowaneList.get(j));
                    //upload dane
                }

                for(int j = 0; j < daneSuroweList.size(); j++)
                {
                    max_id_dane_surowe++;

                    AP apo = database.getAPbyId(daneSuroweList.get(j).getFk_id_ap());
                    String mac = apo.getMac();
                    int ap = exConnection.getIdApByMac(mac);

                    daneSuroweList.get(j).setId(max_id_dane_surowe);
                    daneSuroweList.get(j).setFk_id_punkt_pomiarowy(punkty.get(i).getId());
                    daneSuroweList.get(j).setFk_id_ap(ap);
                    daneSuroweList.get(j).setFk_id_urzadzenie(device.getId());
                    exConnection.createFieldDaneSurowe(daneSuroweList.get(j));
                }
            }

            max_id_dane_surowe = exConnection.getMaxIdFrom("DANE_SUROWE");
            max_id_dane_zagregowane = exConnection.getMaxIdFrom("DANE_ZAGREGOWANE");


        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pomiar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
