package com.example.arkadio.wifimapping;

public class Pomieszczenie
{
    private int id;
    private int fk_id_budynek;
    private int id_E;
    private int id_N;
    private int id_S;
    private int id_W;
    private int liczba_pomiarow;
    private String nazwa;
    private float pionowy_odstep_pomiaru;
    private float poziomy_odstep_pomiaru;
    private float rozmiar_x;
    private float rozmiar_y;

  public int getFk_id_budynek()
  {
    return this.fk_id_budynek;
  }

  public int getId()
  {
    return this.id;
  }

  public int getId_E()
  {
    return this.id_E;
  }

  public int getId_N()
  {
    return this.id_N;
  }

  public int getId_S()
  {
    return this.id_S;
  }

  public int getId_W()
  {
    return this.id_W;
  }

  public int getLiczba_pomiarow()
  {
    return this.liczba_pomiarow;
  }

  public String getNazwa()
  {
    return this.nazwa;
  }

  public float getPionowy_odstep_pomiaru()
  {
    return this.pionowy_odstep_pomiaru;
  }

  public float getPoziomy_odstep_pomiaru()
  {
    return this.poziomy_odstep_pomiaru;
  }

  public float getRozmiar_x()
  {
    return this.rozmiar_x;
  }

  public float getRozmiar_y()
  {
    return this.rozmiar_y;
  }

  public void setFk_id_budynek(int paramInt)
  {
    this.fk_id_budynek = paramInt;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setId_E(int paramInt)
  {
    this.id_E = paramInt;
  }

  public void setId_N(int paramInt)
  {
    this.id_N = paramInt;
  }

  public void setId_S(int paramInt)
  {
    this.id_S = paramInt;
  }

  public void setId_W(int paramInt)
  {
    this.id_W = paramInt;
  }

  public void setLiczba_pomiarow(int paramInt)
  {
    this.liczba_pomiarow = paramInt;
  }

  public void setNazwa(String paramString)
  {
    this.nazwa = paramString;
  }

  public void setPionowy_odstep_pomiaru(float paramFloat)
  {
    this.pionowy_odstep_pomiaru = paramFloat;
  }

  public void setPoziomy_odstep_pomiaru(float paramFloat)
  {
    this.poziomy_odstep_pomiaru = paramFloat;
  }

  public void setRozmiar_x(float paramFloat)
  {
    this.rozmiar_x = paramFloat;
  }

  public void setRozmiar_y(float paramFloat)
  {
    this.rozmiar_y = paramFloat;
  }
}

/* Location:           C:\Users\Arkadio\Desktop\APKtoJava_RC22\tools\classes-dex2jar.jar
 * Qualified Name:     com.example.arkadio.naviwifi.Pomieszczenie
 * JD-Core Version:    0.6.0
 */