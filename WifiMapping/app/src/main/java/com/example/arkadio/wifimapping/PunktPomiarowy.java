package com.example.arkadio.wifimapping;

/**
 * Created by Arkadio on 2015-01-12.
 */
public class PunktPomiarowy {

    private int id;
    private int fk_id_budynek;
    private int fk_id_pomieszczenie;
    private float pozycja_x;
    private float pozycja_y;
    private int graniczny;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFk_id_budynek() {
        return fk_id_budynek;
    }

    public void setFk_id_budynek(int fk_id_budynek) {
        this.fk_id_budynek = fk_id_budynek;
    }

    public int getFk_id_pomieszczenie() {
        return fk_id_pomieszczenie;
    }

    public void setFk_id_pomieszczenie(int fk_id_pomieszczenie) {
        this.fk_id_pomieszczenie = fk_id_pomieszczenie;
    }

    public float getPozycja_x() {
        return pozycja_x;
    }

    public void setPozycja_x(float pozycja_x) {
        this.pozycja_x = pozycja_x;
    }

    public float getPozycja_y() {
        return pozycja_y;
    }

    public void setPozycja_y(float pozycja_y) {
        this.pozycja_y = pozycja_y;
    }

    public int getGraniczny() {
        return graniczny;
    }

    public void setGraniczny(int graniczny) {
        this.graniczny = graniczny;
    }
}
