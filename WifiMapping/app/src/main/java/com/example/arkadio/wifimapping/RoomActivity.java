package com.example.arkadio.wifimapping;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class RoomActivity extends ActionBarActivity {

    Button b_do_pomiaru;
    EditText inputRoomName;
    EditText inputRozmiarX;
    EditText inputRozmiarY;
    EditText inputLiczbaPomiarow;
    EditText inputPoziomyOdstepPomiaru;
    EditText inputPionowyOdstepPomiaru;
    EditText inputN;
    EditText inputS;
    EditText inputE;
    EditText inputW;

    List<Pomieszczenie> pomieszczenia;
    int id_budynek;
    //Stworzenie buttona, który umożliwi nam polaczenie z internetem i zczytanie pomieszczen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        b_do_pomiaru = (Button) findViewById(R.id.button4);
        inputRoomName = (EditText) findViewById(R.id.nazwaPomieszczenie);
        inputRozmiarX = (EditText) findViewById(R.id.szerokosc);
        inputRozmiarY = (EditText) findViewById(R.id.dlugosc);
        inputLiczbaPomiarow = (EditText) findViewById(R.id.liczba);
        inputPoziomyOdstepPomiaru = (EditText) findViewById(R.id.editText6);
        inputPionowyOdstepPomiaru = (EditText) findViewById(R.id.editText7);
        inputN = (EditText) findViewById(R.id.nazwaN);
        inputS = (EditText) findViewById(R.id.nazwaS);
        inputE = (EditText) findViewById(R.id.nazwaE);
        inputW = (EditText) findViewById(R.id.nazwaW);


        Intent intent = getIntent();
        if (null != intent)
        {
            id_budynek = intent.getIntExtra("id_budynek", 0);
        }



        //sprawdzenie czy pomieszczenie istnieje

        b_do_pomiaru.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                String pomieszczenieName = inputRoomName.getText().toString();
                String pomieszczenieRozmiarX = inputRozmiarX.getText().toString();
                String pomieszczenieRozmiarY = inputRozmiarY.getText().toString();
                String pomieszczenieLiczbaPomiarow = inputLiczbaPomiarow.getText().toString();
                String pomieszczenieOdstepX = inputPoziomyOdstepPomiaru.getText().toString();
                String pomieszczenieOdstepY = inputPionowyOdstepPomiaru.getText().toString();
                String pomieszczenieN = inputN.getText().toString();
                String pomieszczenieS = inputS.getText().toString();
                String pomieszczenieE = inputE.getText().toString();
                String pomieszczenieW = inputW.getText().toString();

                /*pomieszczenieName = "test";
                pomieszczenieRozmiarX = "60";
                pomieszczenieRozmiarY = "2.8";
                pomieszczenieLiczbaPomiarow = "39";
                pomieszczenieOdstepX = "5";
                pomieszczenieOdstepY = "1.4";
				*/
                if(pomieszczenieN.equals(""))
                {
                    pomieszczenieN = "0";
                }
                if(pomieszczenieS.equals(""))
                {
                    pomieszczenieS = "0";
                }
                if(pomieszczenieE.equals(""))
                {
                    pomieszczenieE = "0";
                }
                if(pomieszczenieW.equals(""))
                {
                    pomieszczenieW = "0";
                }

                if(pomieszczenieName.equals("") || pomieszczenieRozmiarX.equals("") ||
                        pomieszczenieRozmiarY.equals("") ||
                        pomieszczenieLiczbaPomiarow.equals("") ||
                        pomieszczenieOdstepX.equals("") ||
                        pomieszczenieOdstepY.equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RoomActivity.this);
                    builder.setTitle("Alert Dialog")
                            .setMessage("Uzupełnij wszystkie pola")
                            .setCancelable(false)
                            .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                else
                {
                    //Sprawdzenie czy pomieszczenie istnieje w bazie wewnetrznej
                    Pomieszczenie room = new Pomieszczenie();
                    room.setNazwa(pomieszczenieName);
                    room.setFk_id_budynek(id_budynek);
                    room.setRozmiar_x(Float.parseFloat(pomieszczenieRozmiarX));
                    room.setRozmiar_y(Float.parseFloat(pomieszczenieRozmiarY));
                    room.setLiczba_pomiarow(Integer.parseInt(pomieszczenieLiczbaPomiarow));
                    room.setPionowy_odstep_pomiaru(Float.parseFloat(pomieszczenieOdstepY));
                    room.setPoziomy_odstep_pomiaru(Float.parseFloat(pomieszczenieOdstepX));
                    room.setId_N(Integer.parseInt(pomieszczenieN));
                    room.setId_S(Integer.parseInt(pomieszczenieS));
                    room.setId_E(Integer.parseInt(pomieszczenieE));
                    room.setId_W(Integer.parseInt(pomieszczenieW));


                    SqLiteDatabaseCRUD database = new SqLiteDatabaseCRUD(getApplicationContext());
                    try {
                        database.createDataBase();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    //Sprawdzenie czy pomieszczenie istnieje w bazie wewnetrznej

                    List<Pomieszczenie> testowaLista;
                    testowaLista = database.getPomieszczenieByBudynek(id_budynek);
                    boolean exist = false;
                    for(int i = 0; i < testowaLista.size(); i++)
                    {
                        if(testowaLista.get(i).getNazwa().equals(pomieszczenieName))
                        {
                            room.setId(testowaLista.get(i).getId());
                            exist = true;

                            break;
                        }
                    }

                    if(!exist)
                    {
                        int id_pomieszczenie = database.getMaxIdFromTable("POMIESZCZENIE");
                        id_pomieszczenie++;
                        room.setId(id_pomieszczenie);
                        database.createFieldPomieszczenie(room);

                        List<PunktPomiarowy> punktyPomiarowe = new ArrayList<PunktPomiarowy>();
                        int max_id_punkt_pomiarowy = database.getMaxIdFromTable("PUNKT_POMIAROWY");

                        int j=0,k=0;
                        int rozmiar_min, rozmiar_max;
                        boolean wiekszy;
                        if(room.getRozmiar_x()/room.getPoziomy_odstep_pomiaru()+1 > (room.getRozmiar_y()/room.getPionowy_odstep_pomiaru())+1)
                        {
                            wiekszy = true;
                            rozmiar_min = (int) ((room.getRozmiar_y()/room.getPionowy_odstep_pomiaru())+1);
                            rozmiar_max = (int) ((room.getRozmiar_x()/room.getPoziomy_odstep_pomiaru())+1);
                        }
                        else
                        {
                            wiekszy = false;
                            rozmiar_min = (int) ((room.getRozmiar_x()/room.getPoziomy_odstep_pomiaru())+1);
                            rozmiar_max = (int) ((room.getRozmiar_y()/room.getPionowy_odstep_pomiaru())+1);
                        }

                        float dlugosc, szerokosc, dlugosc_max, szerokosc_max;

                        dlugosc_max = room.getRozmiar_y();
                        szerokosc_max = room.getRozmiar_y();

                        for(int i = 0; i < room.getLiczba_pomiarow(); i++)
                        {
                            if(j%rozmiar_max == 0 && i != 0)
                            {
                                j = 0;
                            }
                            if(k%rozmiar_min == 0 && i != 0)
                            {
                                k = 0;
                                j++;
                            }

                            PunktPomiarowy pkt = new PunktPomiarowy();
                            pkt.setId(max_id_punkt_pomiarowy+1+i);
                            pkt.setFk_id_budynek(id_budynek);
                            pkt.setFk_id_pomieszczenie(room.getId());

                            szerokosc = room.getPoziomy_odstep_pomiaru() * j;
                            dlugosc = room.getPionowy_odstep_pomiaru()* k;

                            if(wiekszy == true)
                            {
                                pkt.setPozycja_x(szerokosc);
                                pkt.setPozycja_y(dlugosc);
                            }
                            else
                            {
                                pkt.setPozycja_x(dlugosc);
                                pkt.setPozycja_y(szerokosc);
                            }
                            //kolumny zostaną edytowane przy danych surowych

                            //Warunki na graniczność punktu
                            if(szerokosc == 0 || dlugosc == 0 || szerokosc == szerokosc_max || dlugosc == dlugosc_max)
                            {
                                pkt.setGraniczny(1);
                            }
                            else
                            {
                                pkt.setGraniczny(0);
                            }
                            Log.d("Punkt pom: " + i, Float.toString(pkt.getPozycja_x()) + " " + Float.toString(pkt.getPozycja_y()) + " Graniczny " + pkt.getGraniczny());


                            punktyPomiarowe.add(pkt);
                            database.createFieldPunktPomiarowy(pkt);
                            k++;
                        }
                    }

                    database.closeDB();
                    Intent intent = new Intent(getApplicationContext(), PomiarActivity.class);
                    intent.putExtra("id_budynek", id_budynek);
                    intent.putExtra("id_pomieszczenie", room.getId());
                    startActivity(intent);

                }
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
