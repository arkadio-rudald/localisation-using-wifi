package com.example.arkadio.wifimapping;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqLiteDatabaseCRUD extends SQLiteOpenHelper
{
    private static final String AP = "AP";
    private static final String BUDYNEK = "BUDYNEK";
    private static final String CREATE_TABLE_AP = "CREATE TABLE AP(id INTEGER PRIMARY KEY,ssid varchar(255) NOT NULL,mac varchar(20) NOT NULL,frequency int(5) NOT NULL)";
    private static final String CREATE_TABLE_BUDYNEK = "CREATE TABLE BUDYNEK(id INTEGER PRIMARY KEY,nazwa varchar(255) NOT NULL)";
    private static final String CREATE_TABLE_DANE_SUROWE = "CREATE TABLE DANE_SUROWE(id INTEGER PRIMARY KEY,nr_pomiaru int NOT NULL,fk_id_ap int NOT NULL,fk_id_punkt_pomiarowy int NOT NULL, fk_id_urzadzenie int NOT NULL, rssi int(4) NOT NULL, CONSTRAINT fk_id_ap FOREIGN KEY (fk_id_ap)REFERENCES AP(id),CONSTRAINT fk_id_punkt_pomiarowy FOREIGN KEY (fk_id_punkt_pomiarowy)REFERENCES PUNKT_POMIAROWY(id), CONSTRAINT fk_id_urzadzenie FOREIGN KEY (fk_id_urzadzenie) REFERENCES URZADZENIE(id))";
    private static final String CREATE_TABLE_POMIESZCZENIE = "CREATE TABLE POMIESZCZENIE" +
            "(id INTEGER PRIMARY KEY," +
            "nazwa varchar(255) NOT NULL," +
            "fk_id_budynek int NOT NULL," +
            " rozmiar_x float(8) NOT NULL," +
            " rozmiar_y float(8) NOT NULL," +
            "liczba_pomiarow int(5) NOT NULL," +
            "poziomy_odstep_pomiaru float(8) NOT NULL," +
            "pionowy_odstep_pomiaru float(8) NOT NULL," +
            " id_N int,id_S int,id_E int,id_W int," +
            "CONSTRAINT fk_id_budynek FOREIGN KEY (fk_id_budynek)REFERENCES BUDYNEK(id))";
    private static final String CREATE_TABLE_PUNKT_POMIAROWY = "CREATE TABLE PUNKT_POMIAROWY(id INTEGER PRIMARY KEY,fk_id_pomieszczenie int NOT NULL,fk_id_budynek int NOT NULL, pozycja_x float NOT NULL, pozycja_y float NOT NULL, graniczny tinyint(1) NOT NULL, CONSTRAINT fk_id_pomieszczenie FOREIGN KEY (fk_id_pomieszczenie) REFERENCES POMIESZCZENIE(id), CONSTRAINT fk_id_budynek FOREIGN KEY (fk_id_budynek) REFERENCES BUDYNEK(id))";

    private static final String CREATE_TABLE_DANE_ZAGREGOWANE = "CREATE TABLE DANE_ZAGREGOWANE(id INTEGER PRIMARY KEY, fk_id_punkt_pomiarowy int NOT NULL," +
            "    fk_id_ap int NOT NULL," +
            "    fk_id_urzadzenie int NOT NULL," +
            "    min_rssi int(4) NOT NULL," +
            "    max_rssi int(4) NOT NULL," +
            "    mediana int(4) NOT NULL," +
            "    dominanta int(4) NOT NULL," +
            "    srednia_rssi float(8) NOT NULL," +
            "    sredniaPL3s float(8) NOT NULL," +
            "    sredniaMIN3s float(8) NOT NULL," +
            "    CONSTRAINT fk_id_punkt_pomiarowy FOREIGN KEY (fk_id_punkt_pomiarowy)" +
            "    REFERENCES PUNKT_POMIAROWY(id)," +
            "    CONSTRAINT fk_id_ap FOREIGN KEY (fk_id_ap)" +
            "    REFERENCES AP(id)," +
            "    CONSTRAINT fk_id_urzadzenie FOREIGN KEY (fk_id_urzadzenie) " +
            "    REFERENCES URZADZENIE(id))";

    private static final String CREATE_TABLE_URZADZENIE = "CREATE TABLE URZADZENIE(id INTEGER PRIMARY KEY,mac varchar(20) NOT NULL)";
    private static final String DATABASE_NAME = "wifimapping";
    private static String DATABASE_PATH = "";

    private static final String LOG = "Database Helper";

    private SQLiteDatabase db;
    private final Context myContext;
    private SQLiteDatabase myDataBase;

    public SqLiteDatabaseCRUD(Context paramContext)
    {
        super(paramContext, "wifimapping", null, 1);
        this.myContext = paramContext;
        DATABASE_PATH = Environment.getExternalStorageDirectory() + File.separator + "Database wifimapping" + File.separator;
        Log.d("Database Ex", DATABASE_PATH);
    }

    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try
        {
            String myPath = "/data/data/com.example.arkadio.wifimapping/databases/wifimapping";
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }
        catch(SQLiteCantOpenDatabaseException e)
        {
            return true;
        }

        if(checkDB != null)
        {
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }
        else
        {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            //this.getReadableDatabase();
            this.getWritableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DATABASE_NAME);

        // Path to the just created empty db
        String outFileName = DATABASE_PATH + DATABASE_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }




    public void createFieldAP(AP obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("ssid", obiekt.getSsid());
        values.put("mac", obiekt.getMac());
        values.put("frequency", obiekt.getFrequency());

        db.insert("AP", null, values);
    }

    public void createFieldBudynek(Budynek obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // Przyporzadkowanie wartosci - 'nazwa tabeli -> wartosc'

        values.put("id", obiekt.getId());
        values.put("nazwa", obiekt.getNazwa());

        //ewentualne sprawdzenie poprawnosci dodania
        db.insert("BUDYNEK", null, values);
    }

    public void createFieldDaneSurowe(DaneSurowe obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("nr_pomiaru", obiekt.getNr_pomiaru());
        values.put("fk_id_ap", obiekt.getFk_id_ap());
        values.put("fk_id_punkt_pomiarowy", obiekt.getFk_id_punkt_pomiarowy());
        values.put("fk_id_urzadzenie", obiekt.getFk_id_urzadzenie());
        values.put("rssi", obiekt.getRssi());

        db.insert("DANE_SUROWE", null, values);
    }

    public void createFieldPomieszczenie(Pomieszczenie obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("nazwa", obiekt.getNazwa());
        values.put("fk_id_budynek", obiekt.getFk_id_budynek());
        values.put("rozmiar_x", obiekt.getRozmiar_x());
        values.put("rozmiar_y", obiekt.getRozmiar_y());
        values.put("liczba_pomiarow", obiekt.getLiczba_pomiarow());
        values.put("poziomy_odstep_pomiaru", obiekt.getPoziomy_odstep_pomiaru());
        values.put("pionowy_odstep_pomiaru", obiekt.getPionowy_odstep_pomiaru());
        values.put("id_N", obiekt.getId_N());
        values.put("id_S", obiekt.getId_S());
        values.put("id_E", obiekt.getId_E());
        values.put("id_W", obiekt.getId_W());

        db.insert("POMIESZCZENIE", null, values);
    }

    public void createFieldPunktPomiarowy(PunktPomiarowy obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("fk_id_pomieszczenie", obiekt.getFk_id_pomieszczenie());
        values.put("fk_id_budynek", obiekt.getFk_id_budynek());
        values.put("pozycja_x", obiekt.getPozycja_x());
        values.put("pozycja_y", obiekt.getPozycja_y());
        values.put("graniczny", obiekt.getGraniczny());

        db.insert("PUNKT_POMIAROWY", null, values);
    }

    public void createFieldUrzadzenie(Urzadzenie obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("mac", obiekt.getMac());

        db.insert("URZADZENIE", null, values);
    }

    public void createFieldDaneZagregowane(DaneZagregowane obiekt)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", obiekt.getId());
        values.put("fk_id_ap", obiekt.getFk_id_ap());
        values.put("fk_id_punkt_pomiarowy", obiekt.getFk_id_punkt_pomiarowy());
        values.put("fk_id_urzadzenie", obiekt.getFk_id_urzadzenie());
        values.put("min_rssi", obiekt.getMin_rssi());
        values.put("max_rssi", obiekt.getMax_rssi());
        values.put("mediana", obiekt.getMediana());
        values.put("dominanta", obiekt.getDominanta());
        values.put("srednia_rssi", obiekt.getSrednia_rssi());
        values.put("sredniaPL3s", obiekt.getSredniaPL3s());
        values.put("sredniaMIN3s", obiekt.getSredniaMIN3s());

        db.insert("DANE_ZAGREGOWANE", null, values);
    }

    public void deleteAPs()
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM AP");
    }
    public void deletePunktyPomiarowe()
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM PUNKT_POMIAROWY");
    }

    public void deleteBuildings()
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM BUDYNEK");
    }

    public void deleteAPbyId(int id)
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM AP WHERE id=" + id);
    }

    public void deleteBuilding(int id_building)
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM BUDYNEK WHERE id=" + id_building);
    }


    public void deletePomieszczeniebyBuilding(int id_building)
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM POMIESZCZENIE WHERE fk_id_budynek=" + id_building);
    }

    public void deleteDaneSurowe()
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM DANE_SUROWE");
    }

    public void deleteDaneZagregowane()
    {
        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
        StringBuilder localStringBuilder = new StringBuilder();
        localSQLiteDatabase.execSQL("DELETE FROM DANE_ZAGREGOWANE");
    }

    public AP getAPbyMac(String mac)
    {
        AP accessPoint = new AP();
        SQLiteDatabase db = getReadableDatabase();
        mac = "\'" + mac + "\'";
        String str = "SELECT * FROM AP WHERE mac=" + mac;
        Cursor c = db.rawQuery(str, null);
        if (c.moveToFirst()) {
            do {
                try {
                    accessPoint.setId(c.getInt(c.getColumnIndex("id")));
                    accessPoint.setSsid(c.getString(c.getColumnIndex("ssid")));
                    accessPoint.setMac(c.getString(c.getColumnIndex("mac")));
                    accessPoint.setFrequency(c.getInt(c.getColumnIndex("frequency")));
                    return accessPoint;
                } catch (CursorIndexOutOfBoundsException ex) {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        else
        {
            return null;
        }

    }

    public int getMaxIdFromTable(String tabela)
    {
        int id = 0;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT MAX(id) FROM " + tabela;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst())
        {
            do
            {
                try {

                    id = cursor.getInt(0);
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return 0;
                }
            } while(cursor.moveToNext());
        }
        return id;
    }

    public int getMinIdFromPunktPomiarowyByBudynek(int id_budynek, int id_pomieszczenie)
    {
        int id = 0;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT MIN(id) FROM PUNKT_POMIAROWY WHERE fk_id_budynek = " + id_budynek + " AND fk_id_pomieszczenie = " + id_pomieszczenie;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst())
        {
            do
            {
                try {

                    id = cursor.getInt(0);
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return 0;
                }
            } while(cursor.moveToNext());
        }
        return id;
    }

    public List<Budynek> getAllBudynek()
    {
        ArrayList localArrayList = new ArrayList();
        SQLiteDatabase db = getReadableDatabase();
        Cursor localCursor = db.rawQuery("SELECT  * FROM BUDYNEK", null);
        if (localCursor.moveToFirst())
          do
          {
            Budynek localBudynek = new Budynek();
            localBudynek.setId(localCursor.getInt(localCursor.getColumnIndex("id")));
            localBudynek.setNazwa(localCursor.getString(localCursor.getColumnIndex("nazwa")));
            localArrayList.add(localBudynek);
          }
          while (localCursor.moveToNext());
        return localArrayList;
    }

    public Budynek getBudynek(int id_building)
    {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM BUDYNEK WHERE id = " + id_building;
        Cursor c = db.rawQuery(query, null);

        Budynek budynek = new Budynek();
        if (c.moveToFirst())
        {
            do
            {

                try
                {
                    budynek.setId(c.getInt(c.getColumnIndex("id")));
                    budynek.setNazwa(c.getString(c.getColumnIndex("nazwa")));
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return budynek;
    }


    public Urzadzenie getUrzadzenieByMac(String mac)
    {
        Urzadzenie device = new Urzadzenie();
        SQLiteDatabase db = getReadableDatabase();
        mac = "\'" + mac + "\'";
        String str = "SELECT * FROM URZADZENIE WHERE mac=" + mac;
        Cursor c = db.rawQuery(str, null);
        if (c.moveToFirst()) {
            do {
                try {
                    device.setId(c.getInt(c.getColumnIndex("id")));
                    device.setMac(c.getString(c.getColumnIndex("mac")));
                    return device;
                } catch (CursorIndexOutOfBoundsException ex) {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        else
        {
            return null;
        }
    }


    public List<DaneZagregowane> getDaneZagregowaneByPunktPomiarowy(int id_punkt_pomiarowy)
    {
        List<DaneZagregowane> dane = new ArrayList<DaneZagregowane>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT  * FROM DANE_ZAGREGOWANE WHERE fk_id_punkt_pomiarowy = " + id_punkt_pomiarowy;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                DaneZagregowane dana = new DaneZagregowane();
                dana.setId(c.getInt(c.getColumnIndex("id")));
                dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
                dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
                dana.setFk_id_urzadzenie(c.getInt(c.getColumnIndex("fk_id_urzadzenie")));
                dana.setMin_rssi(c.getInt(c.getColumnIndex("min_rssi")));
                dana.setMax_rssi(c.getInt(c.getColumnIndex("max_rssi")));
                dana.setMediana(c.getInt(c.getColumnIndex("mediana")));
                dana.setDominanta(c.getInt(c.getColumnIndex("dominanta")));
                dana.setSrednia_rssi(c.getInt(c.getColumnIndex("srednia_rssi")));
                dana.setSredniaPL3s(c.getFloat(c.getColumnIndex("sredniaPL3s")));
                dana.setSredniaMIN3s(c.getFloat(c.getColumnIndex("sredniaMIN3s")));
                dane.add(dana);
            }
            while (c.moveToNext());
        return dane;
    }


    public List<DaneSurowe> getDaneSuroweByPunktPomiarowy(int id_punkt_pomiarowy)
    {
        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT  * FROM DANE_SUROWE WHERE fk_id_punkt_pomiarowy = " + id_punkt_pomiarowy;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                DaneSurowe dana = new DaneSurowe();
                dana.setId(c.getInt(c.getColumnIndex("id")));
                dana.setNr_pomiaru(c.getInt(c.getColumnIndex("nr_pomiaru")));
                dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
                dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
                dana.setRssi(c.getInt(c.getColumnIndex("rssi")));
                dane.add(dana);
            }
            while (c.moveToNext());
        return dane;
    }

    public List<DaneSurowe> getDaneSuroweByAP(int fk_id_ap)
    {
        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM DANE_SUROWE WHERE fk_id_ap = " + fk_id_ap;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                DaneSurowe dana = new DaneSurowe();
                dana.setId(c.getInt(c.getColumnIndex("id")));
                dana.setNr_pomiaru(c.getInt(c.getColumnIndex("nr_pomiaru")));
                dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
                dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
                dana.setRssi(c.getInt(c.getColumnIndex("rssi")));
                dane.add(dana);
            }
            while (c.moveToNext());
        return dane;
    }

    public List<DaneSurowe> getDaneSurowe(int id_building)
    {
        ArrayList dane = new ArrayList();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT  * FROM DANE_SUROWE WHERE fk_id_budynek = " + id_building;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
          do
          {
            DaneSurowe dana = new DaneSurowe();
            dana.setId(c.getInt(c.getColumnIndex("id")));
            dana.setNr_pomiaru(c.getInt(c.getColumnIndex("nr_pomiaru")));
            dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
            dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
            dana.setRssi(c.getInt(c.getColumnIndex("rssi")));
            dane.add(dana);
          }
          while (c.moveToNext());
        return dane;
    }


    public List<AP> getAPList()
    {
        List<AP> apList = new ArrayList<AP>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM AP";

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                AP accessPoint = new AP();
                accessPoint.setId(c.getInt(c.getColumnIndex("id")));
                accessPoint.setSsid(c.getString(c.getColumnIndex("ssid")));
                accessPoint.setMac(c.getString(c.getColumnIndex("mac")));
                accessPoint.setFrequency(c.getInt(c.getColumnIndex("frequency")));
                apList.add(accessPoint);
            }
            while (c.moveToNext());
        return apList;
    }

    public List<Integer> getAPFromDaneSuroweByPunktPom(int id_punkt_pomiarowy)
    {
        List<Integer> apList = new ArrayList<Integer>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT DISTINCT fk_id_ap FROM DANE_SUROWE WHERE fk_id_punkt_pomiarowy = " + id_punkt_pomiarowy;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                int dana;
                dana = c.getInt(c.getColumnIndex("fk_id_ap"));
                apList.add(dana);
            }
            while (c.moveToNext());
        return apList;
    }


    public List<Integer> getRssiFromDaneSuroweByPunktPomAP(int id_punkt_pomiarowy, int id_ap)
    {
        List<Integer> rssiList = new ArrayList<Integer>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT rssi FROM DANE_SUROWE WHERE fk_id_ap = " + id_ap + " AND fk_id_punkt_pomiarowy = " + id_punkt_pomiarowy;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                int dana;
                dana = c.getInt(c.getColumnIndex("rssi"));
                rssiList.add(dana);
            }
            while (c.moveToNext());
        return rssiList;
    }

    public List<DaneZagregowane> getDaneZagregowaneByAP(int fk_id_ap)
    {
        List<DaneZagregowane> dane = new ArrayList<DaneZagregowane>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT  * FROM DANE_ZAGREGOWANE WHERE fk_id_ap = " + fk_id_ap;

        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst())
            do
            {
                DaneZagregowane dana = new DaneZagregowane();
                dana.setId(c.getInt(c.getColumnIndex("id")));
                dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
                dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
                dana.setFk_id_urzadzenie(c.getInt(c.getColumnIndex("fk_id_urzadzenie")));
                dana.setMin_rssi(c.getInt(c.getColumnIndex("min_rssi")));
                dana.setMax_rssi(c.getInt(c.getColumnIndex("max_rssi")));
                dana.setMediana(c.getInt(c.getColumnIndex("mediana")));
                dana.setDominanta(c.getInt(c.getColumnIndex("dominanta")));
                dana.setSrednia_rssi(c.getInt(c.getColumnIndex("srednia_rssi")));
                dana.setSredniaPL3s(c.getFloat(c.getColumnIndex("sredniaPL3s")));
                dana.setSredniaMIN3s(c.getFloat(c.getColumnIndex("sredniaMIN3s")));
                dane.add(dana);
            }
            while (c.moveToNext());
        return dane;

    }

    public List<PunktPomiarowy> getPunktPomiarowyByPomieszczenie(int id_pomieszczenie)
    {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM PUNKT_POMIAROWY WHERE fk_id_pomieszczenie = " + id_pomieszczenie;
        Cursor c = db.rawQuery(query, null);

        List<PunktPomiarowy> punkty = new ArrayList<PunktPomiarowy>();

        if (c.moveToFirst())
        {
            do
            {
                try
                {
                    PunktPomiarowy pkt = new PunktPomiarowy();
                    pkt.setId(c.getInt(c.getColumnIndex("id")));
                    pkt.setFk_id_budynek(c.getInt(c.getColumnIndex("fk_id_budynek")));
                    pkt.setFk_id_pomieszczenie(c.getInt(c.getColumnIndex("fk_id_pomieszczenie")));
                    pkt.setPozycja_x(c.getFloat(c.getColumnIndex("pozycja_x")));
                    pkt.setPozycja_y(c.getFloat(c.getColumnIndex("pozycja_y")));
                    pkt.setGraniczny(c.getInt(c.getColumnIndex("graniczny")));
                    punkty.add(pkt);
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return punkty;
    }


    public PunktPomiarowy getPunktPomiarowyByPosition(int id_pomieszczenie, float posX, float posY)
    {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM PUNKT_POMIAROWY WHERE fk_id_pomieszczenie = " + id_pomieszczenie +
                " AND pozycja_x = " + posX + " AND pozycja_y = " + posY;
        Cursor c = db.rawQuery(query, null);

        PunktPomiarowy pkt = new PunktPomiarowy();

        if (c.moveToFirst())
        {
            do
            {
                try
                {
                    pkt.setId(c.getInt(c.getColumnIndex("id")));
                    pkt.setFk_id_budynek(c.getInt(c.getColumnIndex("fk_id_budynek")));
                    pkt.setFk_id_pomieszczenie(c.getInt(c.getColumnIndex("fk_id_pomieszczenie")));
                    pkt.setPozycja_x(c.getFloat(c.getColumnIndex("pozycja_x")));
                    pkt.setPozycja_y(c.getFloat(c.getColumnIndex("pozycja_y")));
                    pkt.setGraniczny(c.getInt(c.getColumnIndex("graniczny")));
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return pkt;
    }

    public PunktPomiarowy getPunktPomiarowybyId(int id)
    {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM PUNKT_POMIAROWY WHERE id = " + id;
        Cursor c = db.rawQuery(query, null);

        PunktPomiarowy pkt = new PunktPomiarowy();
        if (c.moveToFirst())
        {
            do
            {
                try
                {
                    pkt.setId(c.getInt(c.getColumnIndex("id")));
                    pkt.setFk_id_budynek(c.getInt(c.getColumnIndex("fk_id_budynek")));
                    pkt.setFk_id_pomieszczenie(c.getInt(c.getColumnIndex("fk_id_pomieszczenie")));
                    pkt.setPozycja_x(c.getFloat(c.getColumnIndex("pozycja_x")));
                    pkt.setPozycja_y(c.getFloat(c.getColumnIndex("pozycja_y")));
                    pkt.setGraniczny(c.getInt(c.getColumnIndex("graniczny")));
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return pkt;

    }

    public List<PunktPomiarowy> getPunktPomiarowybyAP(int id_ap)
    {
        List<PunktPomiarowy> punkty = new ArrayList<PunktPomiarowy>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT fk_id_punkt_pomiarowy FROM DANE_SUROWE WHERE fk_id_ap = " + id_ap;

        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst())
            do {
                DaneSurowe dana = new DaneSurowe();
                dana.setFk_id_punkt_pomiarowy(c.getInt(c.getColumnIndex("fk_id_punkt_pomiarowy")));
                dane.add(dana);
            }
            while (c.moveToNext());
        PunktPomiarowy pkt;
        for(int i = 0; i < dane.size(); i++)
        {
            pkt = getPunktPomiarowybyId(dane.get(i).getFk_id_punkt_pomiarowy());
            punkty.add(pkt);
        }
        return punkty;
    }

    public AP getAPbyId(int id_ap)
    {
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM AP WHERE id = " + id_ap;
        Cursor c = db.rawQuery(query, null);

        AP accessPoint = new AP();
        if (c.moveToFirst())
        {
            do
            {
                try
                {
                    accessPoint.setId(c.getInt(c.getColumnIndex("id")));
                    accessPoint.setSsid(c.getString(c.getColumnIndex("ssid")));
                    accessPoint.setMac(c.getString(c.getColumnIndex("mac")));
                    accessPoint.setFrequency(c.getInt(c.getColumnIndex("frequency")));
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return accessPoint;
    }


    public List<AP> getAPbyPunktPomiarowy(int id_punkt_pomiarowy)
    {
        List<AP> routery = new ArrayList<AP>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT fk_id_ap FROM DANE_SUROWE WHERE fk_id_punkt_pomiarowy = " + id_punkt_pomiarowy;

        List<DaneSurowe> dane = new ArrayList<DaneSurowe>();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst())
            do {
                DaneSurowe dana = new DaneSurowe();
                dana.setFk_id_ap(c.getInt(c.getColumnIndex("fk_id_ap")));
                dane.add(dana);
            }
            while (c.moveToNext());
        AP accessPoint;
        for(int i = 0; i < dane.size(); i++)
        {
            accessPoint = getAPbyId(dane.get(i).getFk_id_ap());
            routery.add(accessPoint);
        }
        return routery;

    }

    public Pomieszczenie getPomieszczenieById(int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM POMIESZCZENIE WHERE id = " + id;

        Cursor c = db.rawQuery(query, null);

        Pomieszczenie room = new Pomieszczenie();
        if (c.moveToFirst())
        {
            do
            {
                try
                {
                    room.setId(c.getInt(c.getColumnIndex("id")));
                    room.setNazwa(c.getString(c.getColumnIndex("nazwa")));
                    room.setFk_id_budynek(c.getInt(c.getColumnIndex("fk_id_budynek")));
                    room.setRozmiar_x(c.getFloat(c.getColumnIndex("rozmiar_x")));
                    room.setRozmiar_y(c.getFloat(c.getColumnIndex("rozmiar_y")));
                    room.setLiczba_pomiarow(c.getInt(c.getColumnIndex("liczba_pomiarow")));
                    room.setPionowy_odstep_pomiaru(c.getFloat(c.getColumnIndex("pionowy_odstep_pomiaru")));
                    room.setPoziomy_odstep_pomiaru(c.getFloat(c.getColumnIndex("poziomy_odstep_pomiaru")));
                    room.setId_N(c.getInt(c.getColumnIndex("id_N")));
                    room.setId_S(c.getInt(c.getColumnIndex("id_S")));
                    room.setId_E(c.getInt(c.getColumnIndex("id_E")));
                    room.setId_W(c.getInt(c.getColumnIndex("id_W")));
                }
                catch (CursorIndexOutOfBoundsException ex)
                {
                    return null;
                }
            }
            while (c.moveToNext());
        }
        return room;
    }

    public List<Pomieszczenie> getPomieszczenieByBudynek(int id_budynek)
    {
        ArrayList localArrayList = new ArrayList();
        SQLiteDatabase localSQLiteDatabase = getReadableDatabase();
        String str = "SELECT  * FROM POMIESZCZENIE WHERE fk_id_budynek=" + id_budynek;

        Cursor localCursor = localSQLiteDatabase.rawQuery(str, null);
        if (localCursor.moveToFirst())
          do
          {
            Pomieszczenie room = new Pomieszczenie();
            room.setId(localCursor.getInt(localCursor.getColumnIndex("id")));
            room.setNazwa(localCursor.getString(localCursor.getColumnIndex("nazwa")));
            room.setFk_id_budynek(localCursor.getInt(localCursor.getColumnIndex("fk_id_budynek")));
            room.setRozmiar_x(localCursor.getFloat(localCursor.getColumnIndex("rozmiar_x")));
            room.setRozmiar_y(localCursor.getFloat(localCursor.getColumnIndex("rozmiar_y")));
            room.setLiczba_pomiarow(localCursor.getInt(localCursor.getColumnIndex("liczba_pomiarow")));
            room.setPionowy_odstep_pomiaru(localCursor.getFloat(localCursor.getColumnIndex("pionowy_odstep_pomiaru")));
            room.setPoziomy_odstep_pomiaru(localCursor.getFloat(localCursor.getColumnIndex("poziomy_odstep_pomiaru")));
            room.setId_N(localCursor.getInt(localCursor.getColumnIndex("id_N")));
            room.setId_S(localCursor.getInt(localCursor.getColumnIndex("id_S")));
            room.setId_E(localCursor.getInt(localCursor.getColumnIndex("id_E")));
            room.setId_W(localCursor.getInt(localCursor.getColumnIndex("id_W")));
            localArrayList.add(room);
          }
          while (localCursor.moveToNext());
        return localArrayList;
    }

    public void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
        paramSQLiteDatabase.execSQL(CREATE_TABLE_BUDYNEK);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_POMIESZCZENIE);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_AP);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_URZADZENIE);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_PUNKT_POMIAROWY);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_DANE_SUROWE);
        paramSQLiteDatabase.execSQL(CREATE_TABLE_DANE_ZAGREGOWANE);

    }

    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS BUDYNEK");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS AP");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS POMIESZCZENIE");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS DANE SUROWE");
        onCreate(paramSQLiteDatabase);
    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DATABASE_PATH + DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    public void closeDB()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }



    public void updateBudynek(int previous_id, int current_id)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE BUDYNEK SET id = " + current_id + " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updatePomieszczenie(int previous_id, int current_id, int id_budynek)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE POMIESZCZENIE SET id = " + current_id + ", fk_id_budynek= " + id_budynek + " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updatePunktPomiarowy(int previous_id, int current_id, int id_budynek, int id_pomieszczenie)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE PUNKT_POMIAROWY SET id = " + current_id + ", fk_id_budynek= " + id_budynek + ", fk_id_pomieszczenie = " + id_pomieszczenie + " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updateDaneSurowe(int previous_id, int current_id, int fk_id_ap, int fk_id_punkt_pomiarowy, int fk_id_urzadzenie)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE DANE_SUROWE SET id = " + current_id + ", fk_id_ap= " + fk_id_ap + ", fk_id_punkt_pomiarowy = " + fk_id_punkt_pomiarowy + ", fk_id_urzadzenie = "+ fk_id_urzadzenie+  " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updateDaneZagregowane(int previous_id, int current_id, int fk_id_ap, int fk_id_punkt_pomiarowy, int fk_id_urzadzenie)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE DANE_ZAGREGOWANE SET id = " + current_id + ", fk_id_ap= " + fk_id_ap + ", fk_id_punkt_pomiarowy = " + fk_id_punkt_pomiarowy + ", fk_id_urzadzenie = "+ fk_id_urzadzenie+  " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updateAP(int previous_id, int current_id)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE AP SET id = " + current_id + " WHERE id= " + previous_id;
        db.execSQL(query);
    }
    public void updateUrzadzenie(int previous_id, int current_id)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE URZADZENIE SET id = " + current_id + " WHERE id= " + previous_id;
        db.execSQL(query);
    }


}
