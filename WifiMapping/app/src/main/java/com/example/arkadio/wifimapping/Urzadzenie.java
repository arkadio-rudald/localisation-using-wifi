package com.example.arkadio.wifimapping;

/**
 * Created by Arkadio on 2015-01-15.
 */
public class Urzadzenie {

    private int id;
    private String mac;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
